-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 11, 2021 at 03:57 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travelms`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_valid_points` (IN `customer_Points` INT(11))  BEGIN
    DECLARE msg varchar(255);

    IF customer_Points < 0 THEN
        SET msg = 'Invalid Points, Poinst must greater or equal to 0';
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = msg;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `find_flight` (IN `input_flight_brand` VARCHAR(20), IN `input_flight_rank` INT(11), IN `input_flight_aircraft_type` VARCHAR(20), IN `input_flight_depature_place` VARCHAR(20), IN `input_flight_arrival_place` VARCHAR(20), IN `input_flight_date_depart` DATE, IN `input_flight_date_return` DATE, IN `input_flight_price` DECIMAL(8,2))  BEGIN
    SELECT *
    FROM FLIGHT_SERVICE
    WHERE FLIGHT_SERVICE.Brand LIKE CONCAT("%", input_flight_brand, "%") AND
        (FLIGHT_SERVICE.Rank IS NULL) OR (FLIGHT_SERVICE.Rank = input_flight_rank) AND
        FLIGHT_SERVICE.Aircraft_type LIKE CONCAT("%", input_flight_aircraft_type, "%") AND
        FLIGHT_SERVICE.Depature_place LIKE CONCAT("%", input_flight_depature_place, "%") AND
        FLIGHT_SERVICE.Arrival_place LIKE CONCAT("%", input_flight_arrival_place, "%") AND
        (FLIGHT_SERVICE.Date_depart IS NULL) OR (FLIGHT_SERVICE.Date_depart = input_flight_date_depart) AND
        (FLIGHT_SERVICE.Date_return IS NULL) OR (FLIGHT_SERVICE.Date_return = input_flight_date_return) AND
        (FLIGHT_SERVICE.Price IS NULL) OR (FLIGHT_SERVICE.Price = input_flight_price);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hotel` (IN `input_hotel_name` VARCHAR(50), IN `input_hotel_rank` INT(11), IN `input_hotel_city` VARCHAR(100), IN `input_hotel_price` DECIMAL(8,2))  BEGIN
    SELECT *
    FROM HOTEL_SERVICE
    WHERE HOTEL_SERVICE.Hotel_name LIKE CONCAT("%", input_hotel_name, "%") AND
        (HOTEL_SERVICE.Rank IS NULL) OR (HOTEL_SERVICE.Rank = input_hotel_rank) AND
        HOTEL_SERVICE.Hotel_addr LIKE CONCAT("%", input_hotel_city, "%") AND
        (HOTEL_SERVICE.Price IS NULL) OR (HOTEL_SERVICE.Price = input_hotel_price);

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `CUSTOMER`
--

CREATE TABLE `CUSTOMER` (
  `Username` varchar(30) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Password_digest` varchar(255) NOT NULL,
  `Phone_number` varchar(15) NOT NULL,
  `Points` int(11) NOT NULL DEFAULT 0,
  `Level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `CUSTOMER`
--

INSERT INTO `CUSTOMER` (`Username`, `Name`, `Password_digest`, `Phone_number`, `Points`, `Level`) VALUES
('alicatravel', 'Alica Mee', '$2y$10$sbT5Q0TVgNTeo0AkWI.bm.8lVcUsHynXVEKD/kiPpmOrL/eilUjsO', '0919842063', 0, 'casual'),
('gogi', 'gogi', '$2y$10$wDVRQDKh6rZjBP7NPaLGWO1e.ekvD78WdP0JJnnaB1sh6lQ088mVe', 'g', 0, 'casual'),
('gogo', 'gogo', '$2y$10$weC5okqqtoO./JmfHumVY.Q3zKkTKvLDobqahPLE70UVyc4sXMc7O', '', 0, 'casual'),
('s', 's', '$2y$10$f.6VojjUvtei/AeiSbkC1epJqCvnmoQip3jbhZcfLyTIMx6iP3Fra', 's', 0, 'casual');

--
-- Triggers `CUSTOMER`
--
DELIMITER $$
CREATE TRIGGER `before_insert_points_customer` BEFORE INSERT ON `CUSTOMER` FOR EACH ROW BEGIN
        CALL check_valid_points(NEW.Points);

        IF NEW.Points < 50 THEN
            SET NEW.Level = 'casual';
        
        ELSEIF NEW.Points < 200 THEN
            SET NEW.Level = 'bronze';
            
        ELSEIF NEW.Points < 1000 THEN
            SET NEW.Level = 'silver'; 

        ELSEIF NEW.Points < 5000 THEN
            SET NEW.Level = 'gold'; 

        ELSE
            SET NEW.Level = 'platium';  
            
        END IF;
    END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_update_points_customer` BEFORE UPDATE ON `CUSTOMER` FOR EACH ROW BEGIN
        CALL check_valid_points(NEW.Points);

        IF NEW.Points < 50 THEN
            SET NEW.Level = 'casual';
        
        ELSEIF NEW.Points < 200 THEN
            SET NEW.Level = 'bronze';
            
        ELSEIF NEW.Points < 1000 THEN
            SET NEW.Level = 'silver'; 

        ELSEIF NEW.Points < 5000 THEN
            SET NEW.Level = 'gold'; 

        ELSE
            SET NEW.Level = 'platium';  
            
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `FLIGHT_BOOKING`
--

CREATE TABLE `FLIGHT_BOOKING` (
  `BookingID` int(11) NOT NULL,
  `Discount` decimal(8,2) DEFAULT NULL,
  `Price` decimal(8,2) NOT NULL,
  `Payment_method` varchar(10) NOT NULL,
  `Insurance_type` varchar(10) NOT NULL,
  `Seat_class` varchar(10) NOT NULL,
  `No_of_passengers` int(11) NOT NULL,
  `Booked_by` varchar(30) NOT NULL,
  `Belonged_to` int(11) NOT NULL,
  `Book_date` date NOT NULL,
  `Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `FLIGHT_BOOKING`
--

INSERT INTO `FLIGHT_BOOKING` (`BookingID`, `Discount`, `Price`, `Payment_method`, `Insurance_type`, `Seat_class`, `No_of_passengers`, `Booked_by`, `Belonged_to`, `Book_date`, `Status`) VALUES
(3, '0.10', '1440.00', 'Momo', 'A', 'Economy', 4, 'alicatravel', 4, '2021-11-04', 'BOOKED'),
(4, '0.05', '855.71', 'CreditCard', 'Type B', '2ndClass', 3, 'alicatravel', 6, '2021-11-11', 'CHECKING');

--
-- Triggers `FLIGHT_BOOKING`
--
DELIMITER $$
CREATE TRIGGER `before_insert_flight_booking_price` BEFORE INSERT ON `FLIGHT_BOOKING` FOR EACH ROW BEGIN
        DECLARE FLIGHT_price_per_seat decimal(8,2);

        SELECT FLIGHT_SERVICE.Price
        INTO FLIGHT_price_per_seat
        FROM FLIGHT_SERVICE
        WHERE NEW.Belonged_to = FLIGHT_SERVICE.ServiceID;


        IF (NEW.Discount IS NULL) THEN
            SET NEW.Price = (NEW.No_of_passengers * FLIGHT_price_per_seat);
        
        ELSEIF (NEW.Discount <= 1 AND NEW.Discount >= 0) THEN
            SET NEW.Price = (NEW.No_of_passengers * FLIGHT_price_per_seat) * (1 - NEW.Discount);

        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `FLIGHT_LUGGAGE`
--

CREATE TABLE `FLIGHT_LUGGAGE` (
  `FlightID` int(11) NOT NULL,
  `LuggageID` int(11) NOT NULL,
  `Weight` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `FLIGHT_LUGGAGE`
--

INSERT INTO `FLIGHT_LUGGAGE` (`FlightID`, `LuggageID`, `Weight`) VALUES
(4, 1, '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `FLIGHT_SERVICE`
--

CREATE TABLE `FLIGHT_SERVICE` (
  `ServiceID` int(11) NOT NULL,
  `Rank` int(11) NOT NULL,
  `No_of_seats` int(11) NOT NULL,
  `No_available_seats` int(11) NOT NULL,
  `Brand` varchar(20) NOT NULL,
  `Aircraft_type` varchar(20) NOT NULL,
  `Price` decimal(8,2) NOT NULL,
  `Date_depart` date NOT NULL,
  `Date_return` date DEFAULT NULL,
  `Depature_place` varchar(20) NOT NULL,
  `Arrival_place` varchar(20) NOT NULL,
  `Provided_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `FLIGHT_SERVICE`
--

INSERT INTO `FLIGHT_SERVICE` (`ServiceID`, `Rank`, `No_of_seats`, `No_available_seats`, `Brand`, `Aircraft_type`, `Price`, `Date_depart`, `Date_return`, `Depature_place`, `Arrival_place`, `Provided_by`) VALUES
(1, 9, 30, 30, 'VietnamAirline', 'Boeing737', '400.00', '2021-10-30', '2021-11-09', 'HCM', 'HN', 'Khoi2001'),
(2, 8, 40, 33, 'VietjetAir', 'Boeing747', '400.00', '2021-11-01', '2021-11-09', 'Da Nang', 'Quang Nam', 'PNHuy2001'),
(3, 9, 50, 30, 'VietnamAirline', 'Boeing757', '400.00', '2021-10-31', '2021-11-09', 'HCM', 'Ha Tay', 'Khoi2001'),
(4, 8, 30, 30, 'VietjetAir', 'Boeing767', '400.00', '2021-11-01', '2021-11-09', 'Dong Thap', 'Vinh Long', 'PNHuy2001'),
(5, 10, 30, 30, 'VietjetAir', 'Boeing777', '40.25', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(6, 8, 50, 50, 'Travelokay', 'Boeing737', '300.25', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(7, 10, 50, 50, 'Travelokay', 'Boeing737', '400.25', '2021-12-09', '2021-12-12', 'Da Nang', 'Quang Nam', 'Khoi2001'),
(8, 10, 60, 60, 'OkayYahoo', 'Boeing787', '200.25', '2021-12-09', '2021-12-12', 'Thai Binh', 'Ha Noi', 'Khoi2001'),
(9, 10, 30, 30, 'UK', 'Io89', '78.65', '2021-12-09', '2021-12-12', 'Da Nang', 'Quang Binh', 'Khoi2001'),
(10, 10, 100, 100, 'Juio', 'Nauvi', '356.76', '2021-12-09', '2021-12-12', 'Quang Binh', 'Quang Ngai', 'Khoi2001'),
(11, 9, 40, 40, 'Kio', 'Huy20', '167.65', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(12, 8, 60, 60, 'Wuwi', 'Boi90', '376.45', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(13, 10, 80, 80, 'Jijuo', 'Neuo67', '345.67', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(14, 10, 60, 60, 'Trone', 'Finlane', '386.57', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(15, 9, 80, 80, 'Jyy', 'GHI67', '89.56', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(16, 9, 40, 40, 'h78hk', 'Boeing737', '140.78', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(17, 9, 40, 40, 'Hjui178', 'Boeing737', '178.78', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(18, 9, 40, 40, 'jkui78', 'Boeing737', '180.56', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(19, 9, 40, 40, 'Huyyu', 'Boeing737', '200.56', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(20, 9, 40, 40, 'Ky92769', 'Boeing737', '781.20', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(21, 9, 40, 40, 'OkOk', 'Boeing737', '278.46', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(22, 9, 40, 40, 'Hjjhiy', 'Boeing737', '456.20', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001'),
(23, 9, 40, 40, 'Htqur', 'Boeing737', '378.50', '2021-12-09', '2021-12-12', 'Ho Chi Minh City', 'Ha Noi', 'Khoi2001');

-- --------------------------------------------------------

--
-- Table structure for table `HOTEL_BOOKING`
--

CREATE TABLE `HOTEL_BOOKING` (
  `BookingID` int(11) NOT NULL,
  `Discount` decimal(8,2) DEFAULT NULL,
  `Price` decimal(8,2) NOT NULL,
  `Payment_method` varchar(10) NOT NULL,
  `Insurance_type` varchar(10) NOT NULL,
  `No_of_guests` int(11) NOT NULL,
  `No_of_rooms` int(11) NOT NULL,
  `Room_type` varchar(10) NOT NULL,
  `Checkin_date` date NOT NULL,
  `Checkout_date` date NOT NULL,
  `Booked_by` varchar(30) NOT NULL,
  `Belonged_to` int(11) NOT NULL,
  `Book_date` date NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HOTEL_BOOKING`
--

INSERT INTO `HOTEL_BOOKING` (`BookingID`, `Discount`, `Price`, `Payment_method`, `Insurance_type`, `No_of_guests`, `No_of_rooms`, `Room_type`, `Checkin_date`, `Checkout_date`, `Booked_by`, `Belonged_to`, `Book_date`, `Status`) VALUES
(1, '0.05', '190.00', 'Momo', 'A', 4, 2, 'Casual', '2021-11-10', '2021-11-15', 'alicatravel', 1, '2021-11-04', 'BOOKED'),
(2, '0.00', '400.00', 'Cash', 'B', 4, 2, 'Casual', '2021-11-16', '2021-11-20', 'alicatravel', 2, '2021-11-04', 'BOOKED');

--
-- Triggers `HOTEL_BOOKING`
--
DELIMITER $$
CREATE TRIGGER `before_insert_hotel_booking_price` BEFORE INSERT ON `HOTEL_BOOKING` FOR EACH ROW BEGIN
        DECLARE hotel_price_per_room decimal(8,2);

        SELECT HOTEL_SERVICE.Price
        INTO hotel_price_per_room
        FROM HOTEL_SERVICE
        WHERE NEW.Belonged_to = HOTEL_SERVICE.ServiceID;


        IF (NEW.Discount IS NULL) THEN
            SET NEW.Price = (NEW.No_of_rooms * hotel_price_per_room);
        
        ELSEIF (NEW.Discount <= 1 AND NEW.Discount >= 0) THEN
            SET NEW.Price = (NEW.No_of_rooms * hotel_price_per_room) * (1 - NEW.Discount);

        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `HOTEL_FEATURES`
--

CREATE TABLE `HOTEL_FEATURES` (
  `HotelID` int(11) NOT NULL,
  `Features` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HOTEL_FEATURES`
--

INSERT INTO `HOTEL_FEATURES` (`HotelID`, `Features`) VALUES
(1, 'bathroom'),
(1, 'Hot bath'),
(1, 'Luxury room'),
(1, 'pool'),
(1, 'Wifi'),
(2, 'bathroom'),
(2, 'hot water'),
(2, 'Room Food service'),
(2, 'Wifi'),
(3, 'bathroom'),
(3, 'Wifi'),
(4, 'balcony'),
(4, 'large'),
(4, 'Wifi'),
(5, 'double bed'),
(6, 'balcony'),
(7, 'bathroom'),
(8, 'hot water'),
(9, 'VIP');

-- --------------------------------------------------------

--
-- Table structure for table `HOTEL_NEARBY_SERVICES`
--

CREATE TABLE `HOTEL_NEARBY_SERVICES` (
  `HotelID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Distance` decimal(8,2) NOT NULL,
  `Info` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HOTEL_NEARBY_SERVICES`
--

INSERT INTO `HOTEL_NEARBY_SERVICES` (`HotelID`, `Name`, `Address`, `Distance`, `Info`) VALUES
(1, 'KFC', '400 This street, Known city, Good country', '782.43', 'Fried chicken'),
(1, 'Safari', '380 This street, Known city, Good country', '128.23', 'Watch animals'),
(1, 'TheCoffeeHouse', '350 This street, Known city, Good country', '126.23', 'Additive drink'),
(2, 'Milk farm', '345 Amazon street, Known city, Good country', '126.23', 'Good for health'),
(3, 'KFC', '345 YouTube street, Known city, Good country', '12.63', 'Fried chicken');

-- --------------------------------------------------------

--
-- Table structure for table `HOTEL_SERVICE`
--

CREATE TABLE `HOTEL_SERVICE` (
  `ServiceID` int(11) NOT NULL,
  `Rank` int(11) NOT NULL,
  `Hotel_addr` varchar(100) NOT NULL,
  `Hotel_name` varchar(50) NOT NULL,
  `Price` decimal(8,2) NOT NULL,
  `No_of_rooms` int(11) NOT NULL,
  `No_available_rooms` int(11) NOT NULL,
  `Provided_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HOTEL_SERVICE`
--

INSERT INTO `HOTEL_SERVICE` (`ServiceID`, `Rank`, `Hotel_addr`, `Hotel_name`, `Price`, `No_of_rooms`, `No_available_rooms`, `Provided_by`) VALUES
(1, 5, '345 This street, Known city, Good country', 'The east gem', '100.00', 400, 100, 'Khoi2001'),
(2, 4, '678 Google, Mountview city, The country', 'The engine', '200.00', 400, 100, 'Khoi2001'),
(3, 5, '678 Apple, Bottom city, America country', 'The elegant hotel', '300.00', 400, 50, 'PNHuy2001'),
(4, 3, '678 Netflix, Lakeview city, America country', 'The amazing experience', '400.00', 400, 100, 'PNHuy2001'),
(5, 9, '540 CMT8 Street, Ward 10, Ho Chi Minh City', 'Meta', '9.00', 25, 14, 'Khoi2001'),
(6, 2, '98 Pham Hung Street, Ward 8, Ho Chi Minh City', 'Alphabet', '8.00', 20, 12, 'Khoi2001'),
(7, 4, '78 Ly Thai To Street, Ward 3, Ho Chi Minh City', 'Amazon', '13.00', 20, 19, 'Khoi2001'),
(8, 6, '1 Le Loi Street, Ward 9, Ho Chi Minh City', 'Alibaba', '11.00', 30, 20, 'Khoi2001'),
(9, 8, '5 Quang Trung Street, Ward Go Vap, Ho Chi Minh City', 'Tiki', '12.00', 40, 10, 'Khoi2001'),
(10, 10, '100 Nguyen Hue Street, Ward 1, Ho Chi Minh City', 'Shoppee', '16.00', 50, 0, 'Khoi2001');

-- --------------------------------------------------------

--
-- Table structure for table `PARTNER`
--

CREATE TABLE `PARTNER` (
  `Username` varchar(30) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Password_digest` varchar(255) NOT NULL,
  `Info` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PARTNER`
--

INSERT INTO `PARTNER` (`Username`, `Name`, `Password_digest`, `Info`) VALUES
('123', '546', '123', '56'),
('Khoi2001', 'Khoi', '7957387283jhgjhdk2732sghdjg', 'This is a test'),
('long.hothiengo', 'gogo', '$2y$10$k2ipX.Fzg7xgHFcIPahbxuCJgKi8PfcsbzH7wCJnDMqttDsAhtQRy', 'hihi'),
('PNHuy2001', 'Huy', 'hgjhgj739hgdjghj2424', 'This is another test'),
('r', 'r', '$2y$10$Pf2Uy.gS6v5pVO4WD0KzhO4i.gJ4SBZ4FB06yMIAy8ejpY7CQBfr2', 'u');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CUSTOMER`
--
ALTER TABLE `CUSTOMER`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `FLIGHT_BOOKING`
--
ALTER TABLE `FLIGHT_BOOKING`
  ADD PRIMARY KEY (`BookingID`),
  ADD KEY `fk_FLIGHT_BOOKING_Booked_by` (`Booked_by`),
  ADD KEY `fk_FLIGHT_BOOKING_Belonged_to` (`Belonged_to`);

--
-- Indexes for table `FLIGHT_LUGGAGE`
--
ALTER TABLE `FLIGHT_LUGGAGE`
  ADD PRIMARY KEY (`FlightID`,`LuggageID`);

--
-- Indexes for table `FLIGHT_SERVICE`
--
ALTER TABLE `FLIGHT_SERVICE`
  ADD PRIMARY KEY (`ServiceID`),
  ADD KEY `fk_FLIGHT_SERVICE_Provided_by` (`Provided_by`);

--
-- Indexes for table `HOTEL_BOOKING`
--
ALTER TABLE `HOTEL_BOOKING`
  ADD PRIMARY KEY (`BookingID`),
  ADD KEY `fk_HOTEL_BOOKING_Booked_by` (`Booked_by`),
  ADD KEY `fk_HOTEL_BOOKING_Belonged_to` (`Belonged_to`);

--
-- Indexes for table `HOTEL_FEATURES`
--
ALTER TABLE `HOTEL_FEATURES`
  ADD PRIMARY KEY (`HotelID`,`Features`);

--
-- Indexes for table `HOTEL_NEARBY_SERVICES`
--
ALTER TABLE `HOTEL_NEARBY_SERVICES`
  ADD PRIMARY KEY (`HotelID`,`Name`);

--
-- Indexes for table `HOTEL_SERVICE`
--
ALTER TABLE `HOTEL_SERVICE`
  ADD PRIMARY KEY (`ServiceID`),
  ADD KEY `fk_HOTEL_SERVICE_Provided_by` (`Provided_by`);

--
-- Indexes for table `PARTNER`
--
ALTER TABLE `PARTNER`
  ADD PRIMARY KEY (`Username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `FLIGHT_BOOKING`
--
ALTER TABLE `FLIGHT_BOOKING`
  ADD CONSTRAINT `fk_FLIGHT_BOOKING_Belonged_to` FOREIGN KEY (`Belonged_to`) REFERENCES `FLIGHT_SERVICE` (`ServiceID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_FLIGHT_BOOKING_Booked_by` FOREIGN KEY (`Booked_by`) REFERENCES `CUSTOMER` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `FLIGHT_LUGGAGE`
--
ALTER TABLE `FLIGHT_LUGGAGE`
  ADD CONSTRAINT `fk_FLIGHT_LUGGAGE_FlightID` FOREIGN KEY (`FlightID`) REFERENCES `FLIGHT_BOOKING` (`BookingID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `FLIGHT_SERVICE`
--
ALTER TABLE `FLIGHT_SERVICE`
  ADD CONSTRAINT `fk_FLIGHT_SERVICE_Provided_by` FOREIGN KEY (`Provided_by`) REFERENCES `PARTNER` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HOTEL_BOOKING`
--
ALTER TABLE `HOTEL_BOOKING`
  ADD CONSTRAINT `fk_HOTEL_BOOKING_Belonged_to` FOREIGN KEY (`Belonged_to`) REFERENCES `HOTEL_SERVICE` (`ServiceID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_HOTEL_BOOKING_Booked_by` FOREIGN KEY (`Booked_by`) REFERENCES `CUSTOMER` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HOTEL_FEATURES`
--
ALTER TABLE `HOTEL_FEATURES`
  ADD CONSTRAINT `fk_HOTEL_FEATURES_HotelID` FOREIGN KEY (`HotelID`) REFERENCES `HOTEL_SERVICE` (`ServiceID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HOTEL_NEARBY_SERVICES`
--
ALTER TABLE `HOTEL_NEARBY_SERVICES`
  ADD CONSTRAINT `fk_HOTEL_NEARBY_SERVICES_HotelID` FOREIGN KEY (`HotelID`) REFERENCES `HOTEL_SERVICE` (`ServiceID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HOTEL_SERVICE`
--
ALTER TABLE `HOTEL_SERVICE`
  ADD CONSTRAINT `fk_HOTEL_SERVICE_Provided_by` FOREIGN KEY (`Provided_by`) REFERENCES `PARTNER` (`Username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
