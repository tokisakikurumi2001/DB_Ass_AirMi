<html>
    <script type="text/javascript" src="./register.js"></script>

    <div id="regiser-container" class="container">
        <div class="shadow p-3 bg-body rounded">
            <form method="POST">
                <div id="select-register-div" style="text-align: center; margin:10px">
                    <h4>You want to register as</h4>
                    <input type="radio" name="select-stakeholder" value="customer" checked="true" onclick="stakeholder_chooser();" />Customer
                    <input type="radio" name="select-stakeholder" value="partner" onclick="stakeholder_chooser();" />Partner
                </div>
                <div id="input-register-div">
                    <div class="row justify-content-md-center">
                        <div class="col-md-3">
                            <label class="form-label" for="username">Username</label>
                            <input id="username-input-register" type="text" class="form-control" placeholder="enter username" name="username" required>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-3">
                            <label class="form-label" for="password">Password</label>
                            <input id="pw-input-register" type="password" class="form-control" placeholder="enter password" name="pw" required>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-3">
                            <label class="form-label" for="name">Name</label>
                            <input id="name-input-register" type="text" class="form-control" placeholder="enter name" name="name" required>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-3">
                            <label class="form-label" for="phone">Phone number</label>
                            <input id="phone-input-register" type="text" class="form-control" placeholder="enter phone number" name="phone" required>
                        </div>
                    </div>
                </div>
                <div class="d-grid gap-2 col-2 mx-auto my-1">
                    <br><button id="button-register" type="submit" name="page" value="register" class="btn btn-primary">Register</button>
                </div>
            </form>
        </div>
    </div>
</html>

<?php
    $stakeholder = null;
    if (isset($_POST["select-stakeholder"])) {
        require 'dbaccess.php';
        db_connect();

        $stakeholder = $_POST["select-stakeholder"];
        
        $username = null;
        if (isset($_POST["username"])) {
            $username = $_POST["username"];
        }
        
        $ps = null;
        if (isset($_POST["pw"])) {
            $ps = $_POST["pw"];
        }
    
        $name = null;
        if (isset($_POST["name"])) {
            $name = $_POST["name"];
        }

        $phone = null;
        if (isset($_POST["phone"])) {
            $phone = $_POST["phone"];
        }

        $info = null;
        if (isset($_POST["info"])) {
            $info = $_POST["info"];
        }
        
        if(db_retrieve_a_stakeholder($stakeholder, $username)[0] == 1) { ?>
            <script>
                alert("Username exists, please try again!");
            </script>
        <?php die();}

        $hashed_ps = password_hash($ps, PASSWORD_BCRYPT);
        $insert_status = db_insert_a_stakeholder($stakeholder, $username, $hashed_ps, $name, $phone, $info);
    
        if ($insert_status) { ?>
            <script>
                alert("Register successfully!");
            </script>
        <?php }
    
        else {
            //echo $insert_status;
        }

        db_close();
    }
?>