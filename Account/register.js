var stakeholder_chooser = () => {
    chosen_mode = document.querySelector('input[name="select-stakeholder"]:checked').value;
    if (chosen_mode == null) {
        return;
    }

    //remove prev element
    document.getElementById("input-register-div").innerHTML = "";


    switch (chosen_mode) {
        case "customer":
            document.getElementById('input-register-div').innerHTML =
            "<div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='username'>Username</label>\
                    <input id='username-input-register' type='text' class='form-control' placeholder='enter username' name='username' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='password'>Password</label>\
                    <input id='pw-input-register' type='password' class='form-control' placeholder='enter password' name='pw' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='name'>Name</label>\
                    <input id='name-input-register' type='text' class='form-control' placeholder='enter name' name='name' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='phone'>Phone number</label>\
                    <input id='phone-input-register' type='text' class='form-control' placeholder='enter phone number' name='phone' required>\
                </div>\
            </div>\
                ";
            break;
        
        case "partner":
            document.getElementById('input-register-div').innerHTML =
            "<div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='username'>Username</label>\
                    <input id='username-input-register' type='text' class='form-control' placeholder='enter username' name='username' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='password'>Password</label>\
                    <input id='pw-input-register' type='password' class='form-control' placeholder='enter password' name='pw' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='name'>Name</label>\
                    <input id='name-input-register' type='text' class='form-control' placeholder='enter name' name='name' required>\
                </div>\
            </div>\
            <div class='row justify-content-md-center'>\
                <div class='col-md-3'>\
                    <label class='form-label' for='info'>Info</label>\
                    <input id='info-input-register' type='text' class='form-control' placeholder='enter info' name='info' required>\
                </div>\
            </div>\
                ";
            break;

        default:
            break;
    }
}