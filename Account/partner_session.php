<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>

    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h3 style="text-align: center;"> Hello, <?php echo $_SESSION["user_info"]["Name"]; ?></h3>
            <div class="row justify-content-center">
                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href='../Flight/add_flight_service.php'>Add a flight service</a>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href='../Hotel/add_hotel_service.php'>Add a hotel service</a>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Account
                    </button>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href="./logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Account information</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <?php
        echo "<b>Name</b>: {$_SESSION["user_info"]["Name"]}<br>";
        echo "<b>Info</b>: {$_SESSION["user_info"]["Info"]}<br>";
    ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <h3>Service information</h3>
    <h4>Hotel</h4>

      <div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped">

          <!--Table head-->
          <thead>
            <tr>
              <th>#</th>
              <th>Rank</th>
              <th>Hotel address </th>
              <th>City</th>
              <th>Country</th>
              <th>Hotel name</th>
              <th>Price </th>
              <th>Number of rooms</th>
              <th>Number of available rooms </th>
            </tr>
          </thead>
          <!--Table head-->

        <!--Table body-->
        <tbody>
    <?php
        require 'dbaccess.php';
        db_connect();

        $retrieved_services = db_get_hotel_service_of_a_partner($_SESSION["user_info"]["Username"]);
        echo "<p>There are <b>{$retrieved_services->num_rows}</b> records </p>";

        while ($service = $retrieved_services->fetch_assoc())
        { ?>
          <tr>
            <th scope="row"> <form action="./partner_show_hotel_booking.php" method="POST"> <button type="submit" class="btn btn-secondary" name="hotel-ServiceID" value= "<?php echo $service['ServiceID']; ?>" ><?php echo $service['ServiceID']; ?></button> </form> </th>
            <td><?php echo $service['Rank']; ?></td>
            <td><?php echo $service['Hotel_addr']; ?> </td>
            <td><?php echo $service['City']; ?>  </td>
            <td><?php echo $service['Country']; ?> </td>
            <td>	<?php echo $service['Hotel_name']; ?> </td>
            <td>	<?php echo $service['Price']; ?> </td>
            <td>		<?php echo $service['No_of_rooms']; ?>  </td>
            <td>		<?php echo $service['No_available_rooms']; ?>  </td>
          </tr>

        <?php }
         db_close();
    ?>

            </tbody>
          <!--Table body-->


        </table>
        <!--Table-->
      </div>

<h4>Flight</h4>

<div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped">

          <!--Table head-->
          <thead>
            <tr>
              <th>#</th>
              <th>Rank</th>
              <th>Number of seats </th>
              <th>Number of available seats </th>
              <th>Brand</th>
              <th>Aircraft type </th>
              <th>Price of 1 seat</th>
              <th>Date depart </th>
              <th>Date return </th>
              <th>Place depart</th>
              <th>Place arrive</th>
            </tr>
          </thead>
          <!--Table head-->

        <!--Table body-->
        <tbody>

<?php
    db_connect();

    $retrieved_services = db_get_flight_service_of_a_partner($_SESSION["user_info"]["Username"]);
    echo "<p>There are <b>{$retrieved_services->num_rows}</b> records </p>";

    while ($service = $retrieved_services->fetch_assoc())
    { ?>

        <tr>
            <th scope="row"> <form action="./partner_show_flight_booking.php" method="POST"> <button type="submit" class="btn btn-secondary" name="flight-ServiceID" value= "<?php echo $service['ServiceID']; ?>" ><?php echo $service['ServiceID']; ?></button> </form> </th>
            <td><?php echo $service['Rank']; ?></td>
            <td><?php echo $service['No_of_seats']; ?> </td>
            <td><?php echo $service['No_available_seats']; ?>  </td>
            <td><?php echo $service['Brand']; ?> </td>
            <td>	<?php echo $service['Aircraft_type']; ?> </td>
            <td>	<?php echo $service['Price']; ?> </td>
            <td>		<?php echo $service['Date_depart']; ?>  </td>
            <td>		<?php echo $service['Date_return']; ?>  </td>
            <td>		<?php echo $service['Depature_place']; ?> </td>
            <td>	<?php echo $service['Arrival_place']; ?> </td>
        </tr>

    <?php }
    db_close();
?>


</tbody>
          <!--Table body-->


        </table>
        <!--Table-->
      </div>

</html>