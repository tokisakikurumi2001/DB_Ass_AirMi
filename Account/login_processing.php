<html>
    <head>
    </head>

    <body>
        <?php
            session_start();

            if($_SESSION["user_info"]["Username"]) {
                if ($_SESSION["stakeholder"] == "customer") {
                    $cookie_name = "stakeholder";
                    $cookie_value = "customer";
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 1), "/"); // 86400 = 1 day

                    $cookie_name = "customer";
                    $cookie_value = $_SESSION["user_info"]["Username"];
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 1), "/"); // 86400 = 1 day
                    
                    include 'customer_session.php';
                    
                }

                else if ($_SESSION["stakeholder"] == "partner") {
                    $cookie_name = "stakeholder";
                    $cookie_value = "partner";
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 1), "/"); // 86400 = 1 day

                    $cookie_name = "partner";
                    $cookie_value = $_SESSION["user_info"]["Username"];
                    setcookie($cookie_name, $cookie_value, time() + (86400 * 1), "/"); // 86400 = 1 day

                    include 'partner_session.php';
                }
            }
            else 
                echo "<h1>Please login first .</h1>";
        ?>
    </body>
<html>