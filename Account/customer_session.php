<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>

    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h3 style="text-align: center;"> Hello, <?php echo $_SESSION["user_info"]["Name"]; ?></h3>
            <div class="row justify-content-center">
                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href="./../Flight/homepage.php">Search flight</a>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href="./../Hotel/main.php">Search hotel</a>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Account
                    </button>
                </div>

                <div class="d-grid gap-2 col-3 mx-auto my-1">
                    <a class='btn btn-primary' href="./logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Account information</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <?php
        echo "<b>Name</b>: {$_SESSION["user_info"]["Name"]}<br>";
        echo "<b>Phone</b>: {$_SESSION["user_info"]["Phone_number"]}<br>";
        echo "<b>Points</b>: {$_SESSION["user_info"]["Points"]}<br>";
        echo "<b>Level</b>: {$_SESSION["user_info"]["Level"]}<br>";
    ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <h3>Booking information</h3>
    <h4>Hotel</h4>

      <div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped">

          <!--Table head-->
          <thead>
            <tr>
              <th>#</th>
              <th>Hotel name</th>
              <th>Hotel address </th>
              <th>City</th>
              <th>Country</th>
              <th>No of guests</th>
              <th>No of rooms </th>
              <th>Room type</th>
              <th>Checkin </th>
              <th>Checkout </th>
              <th>Payment method</th>
              <th>Insurance type</th>
              <th>Discount rate</th>
              <th>Total price</th>
              <th>Book date</th>
              <th>Status </th>

            </tr>
          </thead>
          <!--Table head-->

        <!--Table body-->
        <tbody>
    <?php
        require 'dbaccess.php';
        db_connect();

        $retrieved_bookings = db_get_hotel_bookings($_SESSION["user_info"]["Username"]);
        echo "<p>There are <b>{$retrieved_bookings->num_rows}</b> records </p>";

        while ($booking = $retrieved_bookings->fetch_assoc())
        { ?>
            <tr>
              <th scope="row"> <?php echo $booking['BookingID']; ?> </th>
              <td><?php echo $booking['Hotel_name']; ?></td>
              <td><?php echo $booking['Hotel_addr']; ?> </td>
              <td><?php echo $booking['City']; ?> </td>
              <td><?php echo $booking['Country']; ?> </td>
              <td><?php echo $booking['No_of_guests']; ?>  </td>
              <td><?php echo $booking['Requested_no_of_rooms']; ?> </td>
              <td>	<?php echo $booking['Room_type']; ?> </td>
              <td>	<?php echo $booking['Checkin_date']; ?> </td>
              <td>		<?php echo $booking['Checkout_date']; ?>  </td>
              <td>		<?php echo $booking['Payment_method']; ?>  </td>
              <td>		<?php echo $booking['Insurance_type']; ?> </td>
              <td>	<?php echo $booking['Discount']; ?> </td>
              <td>	<?php echo $booking['Total_price']; ?>   </td>
              <td>	<?php echo $booking['Book_date']; ?>  </td>
              <td>		<?php echo $booking['Status']; ?>  </td>
            </tr>

        <?php }
         db_close();
    ?>

            </tbody>
          <!--Table body-->


        </table>
        <!--Table-->
      </div>

<h4>Flight</h4>

<div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped">

          <!--Table head-->
          <thead>
            <tr>
              <th>#</th>
              <th>Brand </th>
              <th>Aircraft type  </th>
              <th>Departure place </th>
              <th>Arrival place  </th>
              <th>Departure date </th>
              <th>Return date </th>
              <th>No of passengers  </th>
              <th>Seat class </th>
              <th>Payment method</th>
              <th>Insurance type </th>
              <th>Discount rate </th>
              <th>Total price </th>
              <th>Book date  </th>
              <th>Status  </th>
            </tr>
          </thead>
          <!--Table head-->

        <!--Table body-->
        <tbody>

<?php
    db_connect();

    $retrieved_bookings = db_get_flight_bookings($_SESSION["user_info"]["Username"]);
    echo "<p>There are <b>{$retrieved_bookings->num_rows}</b> records </p>";

    while ($booking = $retrieved_bookings->fetch_assoc())
    { ?>

        <tr>
            <th scope="row"> <?php echo $booking['BookingID']; ?> </th>
            <td><?php echo $booking['Brand']; ?></td>
            <td><?php echo $booking['Aircraft_type']; ?> </td>
            <td><?php echo $booking['Depature_place']; ?>  </td>
            <td><?php echo $booking['Arrival_place']; ?> </td>
            <td>	<?php echo $booking['Date_depart']; ?> </td>
            <td>	<?php echo $booking['Date_return']; ?> </td>
            <td>		<?php echo $booking['No_of_passengers']; ?>  </td>
            <td>		<?php echo $booking['Seat_class']; ?>  </td>
            <td>		<?php echo $booking['Payment_method']; ?> </td>
            <td>	<?php echo $booking['Insurance_type']; ?> </td>
            <td>	<?php echo $booking['Discount']; ?>   </td>
            <td>	<?php echo $booking['Total_price']; ?>  </td>
            <td>		<?php echo $booking['Book_date']; ?>  </td>
            <td>		<?php echo $booking['Status']; ?>  </td>
        </tr>

    <?php }
    db_close();
?>


</tbody>
          <!--Table body-->


        </table>
        <!--Table-->
      </div>

</html>