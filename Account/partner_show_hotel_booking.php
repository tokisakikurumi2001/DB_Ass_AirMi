<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>

<?php
if (isset($_POST["hotel-ServiceID"])) {
    $serviceID = $_POST["hotel-ServiceID"];
} 
?>

<a class='btn btn-primary' href='./login_processing.php'>Return back</a>

<h4>Hotel</h4>

<div class="table-responsive text-nowrap">
        <!--Table-->
        <table class="table table-striped">

          <!--Table head-->
          <thead>
            <tr>
              <th>#</th>
              <th> Customer name </th>
              <th> Customer phone </th>
              <th>Number of guests  </th>
              <th>Number of rooms </th>
              <th>Room type</th>
              <th>Checkin date </th>
              <th>Checkout date </th>
              <th>Insurance type </th>
              <th>Payment method</th>
              <th>Discount rate </th>
              <th>Total price </th>
              <th>Book date  </th>
              <th>Status  </th>
            </tr>
          </thead>
          <!--Table head-->

        <!--Table body-->
        <tbody>

<?php
    require "dbaccess.php";
    db_connect();

    $retrieved_bookings = db_get_hotel_bookings_of_service($serviceID);
    echo "<p>There are <b>{$retrieved_bookings->num_rows}</b> records </p>";

    while ($booking = $retrieved_bookings->fetch_assoc())
    { ?>

        <tr>
            <th scope="row"> <?php echo $booking['BookingID']; ?> </th>
            <td><?php echo $booking['Name']; ?></td>
            <td><?php echo $booking['Phone_number']; ?></td>
            <td>		<?php echo $booking['No_of_guests']; ?>  </td>
            <td>		<?php echo $booking['No_of_rooms']; ?>  </td>
            <td>		<?php echo $booking['Room_type']; ?>  </td>
            <td>		<?php echo $booking['Checkin_date']; ?>  </td>
            <td>		<?php echo $booking['Checkout_date']; ?>  </td>
            <td>	<?php echo $booking['Insurance_type']; ?> </td>
            <td>		<?php echo $booking['Payment_method']; ?> </td>
            <td>	<?php echo $booking['Discount']; ?>   </td>
            <td>	<?php echo $booking['Price']; ?>  </td>
            <td>		<?php echo $booking['Book_date']; ?>  </td>
            <td>		<?php echo $booking['Status']; ?>  </td>
        </tr>

    <?php }
    db_close();
?>


</tbody>
          <!--Table body-->


        </table>
        <!--Table-->
      </div>

</html>

