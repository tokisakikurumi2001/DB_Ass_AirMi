<?php
    /*=====CONNECT TO DB SERVER=====*/
    include '../share/dbaccess.php';
    $conn = null;

    function db_connect() {
        global $conn, $servername, $username, $password, $db_name;
        $conn = new mysqli($servername, $username, $password, $db_name);

        if ($conn->connect_error) {
            die("Connection failed");
        }
    }

    function db_close() {
        global $conn;
        $conn->close();
    }

    function db_retrieve_a_stakeholder($type, $username) {
        if (gettype($username) != 'string') {
            return array(0, null);
        }

        //check if username exists in table users
        if ($type == 'customer') {
            $select_code =
                "SELECT * 
                FROM `CUSTOMER` 
                WHERE `Username` = '{$username}';
            ";
        }

        else if ($type == 'partner') {
            $select_code =
                "SELECT * 
                FROM `PARTNER` 
                WHERE `Username` = '{$username}';
            ";
        }

        global $conn;
        $queries_result = $conn->query($select_code);

        if ($queries_result->num_rows <= 0 || $queries_result->num_rows > 1 || $queries_result->num_rows == null) {
            return array(0, null);
        }
        
        $retrieved_user = null;
        if ($user = $queries_result->fetch_assoc()) {
            $retrieved_user = $user;
        }

        return array(1, $retrieved_user);
    }

    function db_insert_a_stakeholder($type, $username, $password, $name, $phone, $info) {
        if (gettype($username) != 'string' || gettype($password) != 'string' || gettype($name) != 'string') {
            return 0;
        }
        
        if ($type == 'customer') {
            $insert_code =
            "INSERT INTO `CUSTOMER`(`Name`, `Username`, `Password_digest`, `Phone_number`) 
            VALUES ('{$name}', '{$username}', '{$password}', '{$phone}');
            ";
        }

        else if ($type == 'partner') {
            $insert_code =
            "INSERT INTO `PARTNER`(`Name`, `Username`, `Password_digest`, `Info`) 
            VALUES ('{$name}', '{$username}', '{$password}', '{$info}');
            ";
        }

        global $conn;
        if ($conn->query($insert_code) !== TRUE) {
            return $conn->error;
        }

        return 1;
    }

    function db_get_hotel_bookings($username) {
        $select_code = 
        "SELECT *, HOTEL_BOOKING.Price AS Total_price, HOTEL_BOOKING.No_of_rooms AS Requested_no_of_rooms
        FROM `HOTEL_BOOKING` JOIN `HOTEL_SERVICE` ON HOTEL_BOOKING.Belonged_to = HOTEL_SERVICE.ServiceID
        WHERE `Booked_by` = '{$username}'
        ORDER BY `BookingID` DESC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

    function db_get_flight_bookings($username) {
        $select_code = 
        "SELECT *, FLIGHT_BOOKING.Price AS Total_price
        FROM `FLIGHT_BOOKING` JOIN `FLIGHT_SERVICE` ON FLIGHT_BOOKING.Belonged_to = FLIGHT_SERVICE.ServiceID
        WHERE `Booked_by` = '{$username}'
        ORDER BY `BookingID` DESC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

    function db_get_flight_service_of_a_partner($username) {
        $select_code = 
        "SELECT *
        FROM `FLIGHT_SERVICE`
        WHERE `Provided_by` = '{$username}'
        ORDER BY `ServiceID` ASC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

    function db_get_hotel_service_of_a_partner($username) {
        $select_code = 
        "SELECT *
        FROM `HOTEL_SERVICE`
        WHERE `Provided_by` = '{$username}'
        ORDER BY `ServiceID` ASC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

    function db_get_flight_bookings_of_service($serviceID) {
        $select_code = 
        "SELECT *
        FROM `FLIGHT_BOOKING` JOIN `CUSTOMER` ON FLIGHT_BOOKING.Booked_by = CUSTOMER.Username
        WHERE `Belonged_to` = '{$serviceID}'
        ORDER BY `BookingID` ASC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

    function db_get_hotel_bookings_of_service($serviceID) {
        $select_code = 
        "SELECT *
        FROM `HOTEL_BOOKING` JOIN `CUSTOMER` ON HOTEL_BOOKING.Booked_by = CUSTOMER.Username
        WHERE `Belonged_to` = '{$serviceID}'
        ORDER BY `BookingID` ASC;
        ";

        global $conn;
        $queries_result = $conn->query($select_code);

        return $queries_result;
    }

?>