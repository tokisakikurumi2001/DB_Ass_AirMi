<!DOCTYPE html>
<html lang="en">
  
    <head>
        <meta charset="utf-8">
        <title>AirMi</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>
  
    <body>
    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h1 style="text-align: center;">Welcome to AirMi</h1>
            <form method="GET">
            <div class="row justify-content-center">
                <div class="d-grid gap-2 col-4 mx-auto my-1">
                    <button type="submit" name="page" value="login" class="btn btn-primary">Login</button>
                </div>

                <div class="d-grid gap-2 col-4 mx-auto my-1">
                    <button type="submit" name="page" value="register" class="btn btn-primary">Register</button>
                </div>
            </form>
            </div>
        </div>
    </div>

        <?php
            if(array_key_exists('page', $_GET)) {
                switch($_GET["page"]) {
                    case "login":
                        include 'login.php';
                        break;

                    case "register":
                        include 'register.php';
                        break;
                    
                    default:
                        include 'notfound.php';
                        break;
                }
            }
        ?>
    </body>
  
</html>