<html>
    <div id="login-container" class="container">
        <div class="shadow p-3 bg-body rounded">
            <form method="POST">
                <div id="select-login-div" style="text-align: center; margin:10px">
                    <h4>You want to login as</h4>
                    <input type="radio" name="select-stakeholder" value="customer" checked="true"/>Customer
                    <input type="radio" name="select-stakeholder" value="partner" />Partner
                </div>

                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="username">Username</label>
                        <input id="username-input-login" type="text" class="form-control" placeholder="enter username" name="username" required>
                    </div>
                </div>

                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="password">Password</label>
                        <input id="pw-input-login" type="password" class="form-control" placeholder="enter password" name="pw" required>
                    </div>
                </div>

                <div class="d-grid gap-2 col-2 mx-auto my-1">
                    <br><button id="button-login" type="submit" name="page" value="login" class="btn btn-primary">Login</button>
                </div>
            </form>
        </div>
    </div>

    <?php
    $stakeholder = null;
    if (isset($_POST["select-stakeholder"])) {
        require 'dbaccess.php';

        db_connect();

        $stakeholder = $_POST["select-stakeholder"];

        if (isset($_POST["username"])) {
            $username = $_POST["username"];
        }

        $ps = null;
        if (isset($_POST["pw"])) {
            $ps = $_POST["pw"];
        }
        
        $retrieved_user = db_retrieve_a_stakeholder($stakeholder, $username);

        if ($retrieved_user[0] == 0) { ?>
            <script>
                alert("Username doesn't exists, please register!");
            </script>
        <?php }

        //password in DB is already hashed, verify ps
        else if (!password_verify($ps, $retrieved_user[1]["Password_digest"])) { ?>
            <script>
                alert("Wrong password and/or username, please try again!");
            </script>
        <?php }

        else {
            session_start();
            $_SESSION["user_info"] = $retrieved_user[1];
            $_SESSION["stakeholder"] = $stakeholder;
            header("Location: login_processing.php");
        }

        db_close();
    }
?>
</html>