<script>
// LINK: https://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function whichInsuranceType(price) {
    price = parseInt(price);
    if (price >= 400) {
        return 'Type A';
    } else if (price >= 300) {
        return 'Type B';
    } else if (price >= 200) {
        return 'Type C';
    } else {
        return 'No plan';
    }
}


function cookieHotel(serviceID, price, partner) {
    createCookie('Hotel_service', serviceID, 1);
    const insuranceType = whichInsuranceType(price);
    createCookie('Insurance_type', insuranceType, 1);
    createCookie('Unit_price', price, 1);
    createCookie('Partner', partner, 1);
}
</script>