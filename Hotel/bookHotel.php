<?php
    include_once 'connectDB.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Hotel Booking | AirMi</title>
</head>
<body>
<div class="container">
    <h1 style="text-align: center; color: dodgerblue"><b>Provide your infomation</b></h1>
<div class="shadow p-3 bg-body rounded">
    <form method = "post" action="<?php echo $_SERVER['PHP_SELF'];?> " style="width: 800px; margin: auto">
        <div class="row" >
            <div class="form-floating mb-3 col">
                <input type="number" class="form-control" id="no_guests" placeholder="name@example.com" name="no_guests" style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">
                <label for="no_guests" class="form-label" style = "padding-left: 20px" style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">Number of people</label>
            </div>
            <div class="form-floating mb-3 col">
                <input type="number" class="form-control" id="no_rooms" placeholder="name@example.com" name="no_rooms" style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">
                <label for="no_rooms" class="form-label" style = "padding-left: 20px">Number of rooms</label>
            </div>
        </div>

        <div class=" justify-content-md-center row">
            <div class="col-md-6 form-floating">
                <input type="date" class="form-control" id="checkin" name="checkin" style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold" required>
                <label class="form-label" for="date_depart" style = "padding-left: 20px">Checkin date</label>
            </div>
            <div class="col-md-6 form-floating">
                <input type="date" class="form-control" id="checkin" name="checkout" style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold" required>
                <label class="form-label" for="date_depart" style = "padding-left: 20px">Checkout date</label>
            </div>
        </div>
        <select class="form-select" aria-label="Default select example" name='room_type'  required style=" margin-top: 10px;height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">
            <option selected style = "font-weight: bold; color: dodgerblue">Room type</option>
            <option value="super vip" style="font-weight: bold; color: dodgerblue">Super VIP</option>
            <option value="vip" style="font-weight: bold; color: dodgerblue">VIP</option>
            <option value="casual" style="font-weight: bold; color: dodgerblue">Casual</option>
        </select>
        <select class="form-select" aria-label="Default select example" name = 'payment'  required style="margin-top: 10px; height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">
            <option selected>Payment method</option>
            <option value="credit" style="font-weight: bold; color: dodgerblue">Credit card</option>
            <option value="momo" style="font-weight: bold; color: dodgerblue">Momo</option>
            <option value="zalopay" style="font-weight: bold; color: dodgerblue">ZaloPay</option>
        </select>

        <div class="col">
            <button type="submit" name="filter" class="btn btn-primary" value="Filter"
                    style = ' background-color: dodgerblue; width: 100px; margin-top: 20px; font-weight: bold; color: white'
                    onClick = 'window.location.href="Red()'>Book</button>
            <a class = 'btn btn-primary' href = '../Account/login_processing.php' style="margin-left:20px; margin-top: 20px; background-color: dodgerblue;font-weight: bold;">Home page</a>
        </div>
    </form>
</div>
    <script type="text/javascript">
        // <!--
        function Red() {
            window.location.href="Account/";
        }
        //-->
    </script>
<?php  include_once "bookInfo.php" ?>
<!---->




</div>
</body>
</html>
