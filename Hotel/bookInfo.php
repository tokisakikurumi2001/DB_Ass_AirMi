<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $checkin = $_POST['checkin'];
    $checkout = $_POST['checkout'];
    $no_guests = $_POST['no_guests'];
    $no_rooms = $_POST['no_rooms'];
    $room_type = $_POST['room_type'];
    $payment = $_POST['payment'];
    $discount = 0.05;

    $bookby = "gogi";
    $serviceID = 0;
    $price = 0;
    $insuranceType = "";
    $partner = 1;
    $cookie_name = 'Hotel_service';
    $status = "CHECKING";
    if(!isset($_COOKIE[$cookie_name])) {
        echo "Cookie named '" . $cookie_name . "' is not set!";
    } else {
        $serviceID = $_COOKIE[$cookie_name];
        $price = $_COOKIE['Unit_price'];
        $insuranceType = $_COOKIE['Insurance_type'];
//        $partner = $_COOKIE['Partner'];
    }
    ?>


    <?php
    $sql = "SELECT MAX(BookingID) AS MaxID FROM `HOTEL_BOOKING`";
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);
    $maxID = 0;
    if ($resultCheck > 0){
        $row = mysqli_fetch_assoc($result);
        $maxID = $row['MaxID']+1;
//        echo $maxID;
    }
//    else echo $sql.'<br>';
    $bookby = $_COOKIE['customer'];
    $sql = 'INSERT INTO `HOTEL_BOOKING`(`BookingID`, `Discount`, `Price`, `Payment_method`, `Insurance_type`, `No_of_guests`, `No_of_rooms`, `Room_type`, `Checkin_date`, `Checkout_date`, `Booked_by`, `Belonged_to`, `Book_date`, `Status`) VALUES
            ( '.$maxID.', '.$discount.', '.$price.', "'.$payment.'", "'.$insuranceType.'", '.$no_guests.', '.$no_rooms.', "'.$room_type.'", "'.$checkin.'", "'.$checkout.'", "'.$bookby.'", '.$serviceID.', '.'NOW()'.', "'.$status.'")';
    //echo $sql;
    $result = mysqli_query($conn, $sql);
    if ($result===TRUE){
        echo '<div class="alert alert-primary" role="alert">
  The partner will contact to you as soon as possible to validate for the booking
</div>'."";
    }
    else echo '<div class="alert alert-danger" role="alert">
  Fill all the required information
</div>'."";
}
?>