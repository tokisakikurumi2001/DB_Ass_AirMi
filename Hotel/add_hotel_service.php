<!-- Add the hotel service -->

<!-- form stuff -->
<!-- action send to insert_hotel.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Hotel Add | AirMi</title>
</head>
<body>
    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h1 style="text-align: center;">New AirMi Hotel Service</h1>
            <form action="./insert_hotel.php" method="POST">
                <!-- RANK -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="rank">Rank</label>
                        <input type="number" class="form-control" id="rank" placeholder="Rank of services" name="rank" value = "9" required>
                    </div>
                </div>
                <!-- NUMBER OF ROOMS -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="no_rooms">Number of rooms</label>
                        <input type="number" class="form-control" id="no_rooms" placeholder="Number of rooms" name="no_rooms" value = "200" required>
                    </div>
                </div>
                <!-- HOTEL NAME -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="hotel_name">Hotel name</label>
                        <input type="text" class="form-control" id="hotel_name" placeholder="Hotel name" name="hotel_name" required>
                    </div>
                </div>
                <!-- CITY -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="city">City</label>
                        <input type="text" class="form-control" id="city" placeholder="City" value = "Ho Chi Minh City" name="city" required>
                    </div>
                </div>
                <!-- COUNTRY -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="country">Country</label>
                        <input type="text" class="form-control" id="country" placeholder="Country" value = "Vietnam" name="country" required>
                    </div>
                </div>
                <!-- HOTEL ADDRESS -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="hotel_addr">Hotel address</label>
                        <input type="text" class="form-control" id="hotel_addr" placeholder="Hotel address" name="hotel_addr" required>
                    </div>
                </div>
                <!-- PRICE -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="price">Price of 1 room</label>
                        <input type="number" step=0.01 class="form-control" id="price" placeholder="Price of 1 room" name="price" required>
                    </div>
                </div>
                <div class="d-grid gap-2 col-2 mx-auto my-1">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <a class='btn btn-secondary py-2' href='../Account/login_processing.php'>Back to homepage</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>