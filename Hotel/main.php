<?php
    include_once 'connectDB.php';
    include_once "cookie.php";
    include_once 'multiSelect.php';
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Hotel Search | AirMi</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
<body>
<div class="container" style = "width: 800px">
    <div class="shadow p-3 bg-body rounded">
        <div >
            <h1 style="text-align: center; color: dodgerblue"><b>Please choose your hotel</b></h1>
            <div class="col" style="text-align: center; margin-top: 40px;">
                <input type = "checkbox" class = "checks btn-check" value = "bathroom" id="option1" autocomplete="off" style="color: dodgerblue; font-weight: bold">
                <label class="btn btn-outline-primary" for="option1" style="font-weight: bold">Bathroom </label>
                <input type = "checkbox" class = "checks btn-check" value = "hot water" id="option2" autocomplete="off">
                <label class="btn btn-outline-primary" for="option2"  style="font-weight: bold">Hot water </label>
                <input type = "checkbox" class = "checks btn-check" value = "pool" id="option3" autocomplete="off">
                <label class="btn btn-outline-primary" for="option3"  style="font-weight: bold">Pool </label>
            </div>
            <div class="col" style="text-align: center; margin-top: 10px;">
                <input type = "checkbox" class = "checks btn-check" value = "balcony" id="option4" autocomplete="off" style="color: cyan">
                <label class="btn btn-outline-primary" for="option4" style="font-weight: bold" >Balcony </label>
                <input type = "checkbox" class = "checks btn-check" value = "large" id="option5" autocomplete="off">
                <label class="btn btn-outline-primary" for="option5" style="font-weight: bold">Large </label>
                <input type = "checkbox" class = "checks btn-check" value = "double bed" id="option6" autocomplete="off">
                <label class="btn btn-outline-primary" for="option6" style="font-weight: bold">Double bed </label>
            </div>


            <form method = "post" action="<?php echo $_SERVER['PHP_SELF'];?>">
                <div class="input-group mb-3" style="margin-top: 20px; height: 50px">
                    <span class="input-group-text" id="inputGroup-sizing-default" style="color: wheat; background-color: dodgerblue"><b>Features</b></span>
                    <input name = 'features' type="text" id='selectedtext' class="form-control" style="color: dodgerblue; font-weight: bold" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>

                <div class="row g-3">
                    <div class="col">
                        <select class="form-select" aria-label="Default select example" name = 'city' style=" height: 60px; border-color: dodgerblue; color: dodgerblue; font-weight: bold">
                            <option style="color: dodgerblue; font-weight: bold" selected>City</option>
                            <option style="color: dodgerblue; font-weight: bold" value="Ha Noi">Ha Noi</option>
                            <option style="color: dodgerblue; font-weight: bold" value="Ho Chi Minh">Ho Chi Minh</option>
                            <option style="color: dodgerblue; font-weight: bold" value="Da Nang">Da Nang</option>
                            <option style="color: dodgerblue; font-weight: bold" value="Long An">Long An</option>
                        </select>

                    </div>
                    <div class="form-floating mb-3 col" style="color: dodgerblue; font-weight: bold; border-color: dodgerblue;">
                        <input type="text" class="form-control" name="available" placeholder = "No of rooms" style="color: dodgerblue; font-weight: bold; border-color: dodgerblue;">
                        <label for="available" class="form-label">Number of rooms</label>
                    </div>
                </div>
                <div class="col">
                    <button type="submit" name="filter" class="btn btn-primary col" value="Filter" style = 'background-color: dodgerblue; width: 120px; margin-top: 20px; font-weight: bold; color: white'>Filter</button>
                    <a class = 'btn btn-primary col' href = '../Account/login_processing.php' style="margin-left:20px; margin-top: 20px; background-color: dodgerblue;font-weight: bold;">Home page</a>
                </div>
            </form>
        </div>

    </div>
</div>

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // collect value of input field
        $name = $_POST['features'];
        $num_available = $_POST['available'];
        $city = $_POST['city'];
        if (empty($name)) {
            $num_available=0;
        }

        $sql = "SELECT *
                FROM HOTEL_SERVICE
                WHERE No_available_rooms > ".$num_available." AND City = ".'"'.$city.'";';

        if(!empty($name)){
            $sql = "SELECT *
                    FROM HOTEL_SERVICE,
                        (SELECT HotelID, GROUP_CONCAT(Features) AS Features
                            FROM HOTEL_FEATURES
                            GROUP BY HotelID
                        ) as fea
                    WHERE HOTEL_SERVICE.ServiceID = fea.HotelID
                    AND Features LIKE "."'%".$name."%'"
                ."AND No_available_rooms > ".$num_available." AND City = ".'"'.$city.'"';
        }
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
?>
    <div class="container">
    <div class = "row">
        <?php
            if($resultCheck > 0) {
                while($row = mysqli_fetch_assoc($result)) {
        ?>    
            <div class="col-md-4">
                <div class="card" style="width: 25rem; align-text: center; margin-top: 1.2rem;">
                    <div class="for-image">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVEhgSEhIYGBgaGBkaGBoYGBoZGBgZGBwaGRoZGhocIS4nHR4sHxkZJjgmKzAxNTU1GiU7QDs0Py40NTEBDAwMEA8QHxISHzcrJSs3NjY0NDY0Nzc0NDQ0NDY0NDQ1Njc2NzQ0NDQ0NDQ0NjE0NDQ0NDY0NDQ0NDQ0NDQ0NP/AABEIALoBEAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQMEBQYCB//EAD8QAAIBAwMCBAMECAYBBAMAAAECEQADIQQSMSJBBRNRYQYycUKBkaEUUmKSscHR8CMzU3KC4bIVotLxFiRD/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAIDAQQF/8QALhEAAgIBAwIFAwIHAAAAAAAAAAECEQMSITFBgQQiMmFxE1GhsfEUM1Ji0eHw/9oADAMBAAIRAxEAPwDCUtJS16JyhRRRQAUUUUAFFFFMYFFFFABRRRQKFFFSV0bm2bggqOfYClbS5NSb4IwrquRSimNFopKWgBaKSlpQCiiigAooooAKKKKACilooASilooASilooAKKKWgdDNLRRQKFFFFABRRRQAUUUlMYLRSUtABRRV94H4Amot7jcZSSQAIgQYzI/pSykoq2EYuTpFAa3/g/gAW2ti9H+Im95IG3cGIEkgfZHpUD4d+GWXVF9QAbdoB5H22mFWOxJ7Gr+8S912uNBJH0A7CPTgVy5sup1F9zoxY63kZDxT4Vu2rkJDoQSrbhGO098/8AcVQspBIIggwR7ivVNPaZQ9siQyblHI3Blh1jvEj76yPjnw7AN/TEshPWv2lY+39g9vStx5t6k+4s8W1xMzRRS10kQpaSigBaKKKACilpKAFooooAKKKKACiiloNEopaKDUFLSUUAM0UUUBQtFJS0AFFFFBghpKU0lBgtAooFMAtbf4IkW5gwHMkAwPlOaxIrefDSX00iKGdAb7l08tmDoVRcsFOzIbJ9qllVxoIz0NM0d24rkqkBVXe59SIX7yBVXacOSw4J/mR99cXLTG24RgpkTP6uS30OK50TqUG3jgccBiP5TmuBRpWdrlbovvC1tufLuGFjA7bgwhge05BHf8KrlsGBctHMQQRg4EiDhhkfjUe6SFMf980+XVUDKWD74Gentz/fakm2lY8Vbop/EfArN8m4oKOoJdAYDAD5gT2Hcc/Xtir9so5QgiCeeY7GvUrbi5uglHKvJGdysDuB9RB/Kqnxfw1L8peAW4sqLg7kYhox9/fv6i+LO06ZLJiT3R5/T2p0723ZLilXUwynkf36960mg+FgoY6hgzE7LaKTkn7bemJPp3zirK1obetsmzcaNTbYqjkdRVf1+7r83uMes1b+Jhq0pnO4NcmEop/W6N7Nw27qFGHYiJHqPUUxXQKFLSUtBgUUUUAFFLRQaFFFFABRRRQAUUUUAMtRSk0UDMKKKKACiiigwQ0UtJQACgUtFAoor1XxV0u+RuAQo4VFVwomFPyFWLEz+zzyYx5Wten6VEjqEnzHIlN21tqGQxYbe3apZOU/ZmSVqvdDGothrbhiQJXc3oMn+Q/7o0RBtgqu0dh7BiB98Zp3UbQjl/lBBIjJ6WwPx/Kl0bsUBZYJ7cADcYEf7Y5rmb8qOtLzs4vL0H6Gp6OhshGSDuMP6Y9Z49qiahehvoanKX/R4jo3ndGe3cdj7j865svBaPJAss3lfJ0h/njKmOAex7xT1q2zKHuCUZiC3ecE/wAa60Vt/JZlYEbmBQnnpGR68j3/ABrmyo8mVfqG6UPERyD+OPvpHW/yh10+Bt9Wtq3vdJ2AAEDIkjp+8xWObUMtwuJVtxbGGUkyK1/iNz/AK2huZkAdW4J3qYWMjgY+tZPV23UjeARJhgIIPdSO0GcfSot1IjkxqtQx4/4m98J5kErvhu5D7TH0xx2n7qpqm6w4H99qhV7Hh5N41ZysSlooroFFooooABRRRQAUUUUAFFFFABSikpaAGaKKKBwooooMCikooAWiiigwKKKKDKFFepWLtxVAS3ILkMd5SEIUFtu4BzM4zzxXlleteH3yiMAC25yJCM23pXpJCnaPlPP3VLLwY1svlEa6QEcxMEEDsTDYPqOcf2F0avsHmfNyfvJMfgYrpTAbGdw244O1jxxOI++u9OgAgGfXM5nqk+szNcnRHYl5mc6gdDfQ1MtowtBg4+YwhwTAzB79sVG1A6G+jU/aRPKBDEPvwBkTGCV/geOahl4Kw5INi4FSBMlhHG2YxI9ff608hTyYKw3XD5E4nbPB4iPeu9M6+SVZJG/LEcAgcMOD7d/ahEfyJkFJbpnM7RmPbBkUju38oZdPgh+KT+jqbnSIGxxHT1ckjjvzWa1KpDm3cJyN6sInqEOoOcmD/GK0+vT/APW6OqVG9T6buFI/gc1m/FtO6v8A4loJxt2ggAYwZM/9mueXq7szJ6X8FJrBgVCqdr+B9f5VBr2fC/y0cAClpBS10ihRRSUALRQKKDaCiiigKCloooASilpKAY1RS0lZY1BRRRWgFFFFBgUUUUAFIKWigBDXouoRG8suQpF8bOvaWYbMxsO7kCJHHOa86Nej6h1Xyw+SbyxCK+0dO0ksZTO7qAnP4pLp3EyLy91+pYKGKuE74JmNsqYaZ4B2muNAECAW23KJAb1IYgke0zXV61ut3FLhQfmP7O0zHqRMj6Unhyp5ai2pVBIAJk4Ygk+5MnPrXF0Oxepj18dLfRqdBTywpXq3fNkECPXgj61xeHSfo1KbbG3IBgMN0Ng44K+vuIqGW62Kw5DSM4stABTfLCcnAkFe4j8K4QL5PzHdubp5Hy8xyDE59qd0dsNbw53bxC4ImBDEcjOJHpmmkQeST5c9fzxx0jpke/Y+tK6tr3Rq6EPxEnyJPQAnSwEEkNzPcVndRBDm3dLjG8N3O4dSznkzP8KvPG9eljTF7mQ3TG05Mkx6Hjk8RGKrH0V3XWfNsqlnK7bjKyIyd+o5MDuME9+9QcZSnsurCatV7Gf1ltm2qqliTAABJJjgAVAa2wmVIiZwcRzNbux4Rp7RDvfuXXX/AE1CrMcy38jUry9MTH6IrSCSXdiTkHOD3r0MOdY4aWcyws890mne5/lqWwTjGBkzPFIRBjv6d8//AFXoVq5ZRQE0dpRtbAJXGcYXjNR//TtG7eY2kKOwgtbfdAIByrQD8o7TTrxX3D6JhCK5itD418OvZttetnzrYOdiw6D9tSZwO/4xVG6EYIrohkjLgnKDjyN0ldEVzVBQooorQFopKKAFopKUUARLGqVzAmfenqZsWSMsqj6CnqjByrcrKuglFLRVSYlBpaSgAooorTAooorDRGr0q/cINtUWZvJum55YzsnaA43kCMQfpmvNW4r0zWpcJtbdoAuoY8suWErOQjbJz1Ej8sTl07k8np7onmyrBwxIEjjBOCCs9pmndIBtEAAQAAvAAMAD1x3GKZuXgtt3e2TBHT78CQBkU/objsga4u1jyIiBOPyjnPrXHvR2dRdSvSY/Vb+FKEIQMHEzCqwPOD0niSQMTml1IlT9G/hRbXoUgtuLgLBBAMckfzqOb0/5Kw5IK4X5e4liDj6EfeePSnLNwrb7ZZhEGflzkdu8H0qTpnPllQAV3AmGg8cFe4+7Fc6S2gWftBpySMATHoZIj76nN8/KGT47kDU2kUL56K7/ADLbbKJ3V7gOC49Bx+dNb7uoOAz4GBhFM49l++rJPDFe4dTfbasYUmBBMku3ue34+geueNIgCWUECACRCgHjag/nFNbat7CEVfhm64O64q8mFUuePqAKnL8NopG+43BHKL6eoNQXvau8OHgjudi/u4kfjSWfBruCSgxnJJ/8aEl0QWycPANPAAu9iP8AMSYP3Vxf+F+n/DuOOIJhhAEdoqGPA7kAb04PY9/upLOi1NgA24wFnY+2Yns0A9sZor2CxCblhgbg29t65Q8CG9O+DVT434ampXzLOxLoGUOEec4I4bmBxmtbpvGkceXqUg8FtsMO3Wn9Me1VPjXwoY8zSONpIJWehh+yR8v5j6U0W47pmbPZnnGs0rI5R1IIMEER/HtUUitJqLBvkW7jMmpQbTbuEdfcbHnJg8d+1UGotMjFWUqRggiCK9DHk1L3OecKGaSlNJVidBSzSVy5xyB9aSc1COpmxjqdI7oFRyXB4yfcR7GYk98U8WjB5gGBmJ4qMfERlzsUeGSK5WUDF5yYx0k/QhTyKkafdMliR9Fj8sikS/EZLEkgTGD6AwKd/Sk9T6zGPypISV80PKLO6K5FxTwa6rqTsiwooopjBKKWigBKWilFAHLjFema6yGa11HF9DG9LYkbP1suIjAzXmtzg16j4nbTdY3i3PnWwu8vJysbdmN0zE4/Opye67k8vp7ol6lnVGNtAzSsAj8x7x64rvRI4QeZ8/JzPJkDj0indWri2xthi0qBtie08mB9ZxS+H6Z1tgXILySYzyxIExyAa4+h2dTi+vS30auRs2AECS3PUNqxzOQRUy8nSfoaRLZZQFgjcCeDIjgYxUsibW34KR53KM6t0HAKK6EgwTvmFwc+lc6bxFGuLZUM7jq2rJlYIEngDcD3mpeo8GQ9WpvbFYgbFbZuMkjrHWTxhSK6u+LpZU29Jp9zdgARuJE/KoLNzyaRwTbvrXzsNdJUNN4bqdQC2ruLZSPkUhmAIyS3yKffqqYmp01nNtd7Y6u57fOf5CKjaXwzWX7ge+wROqFJnmNpCLjjcMkGrK34bpLI/wAW4HYAAhju/wDYnv6zQ75F2K2/487ErbUTmAql2/Pn8KVH1LEdN3j0KenoBVhf+IbSCLdvAx2QfcFn+VRj8RPIgIMdwx9P2hWNrqzexE8nVwOm5O0/bP3faqNqNdqrNvf5buRslXUsM4MsBI796lf/AJM8Aynyk/Ke3/KnNN8QsQC4Q4HEr2nkk1qa+5hF0vj2m1KhL67HMgEnEgx0uIK/QwPrSa7Q63TKX0dzzVjCNEkAdxw/bIg+lWOp0Wk1IBuJsc5DjpbPfeMH/lUTV+F6rTjdpbu9eQuM/wDE9J+og1vJlHnniese/cN24AGaJCiANoAET9O9OXNe9y3suAOVja7fOB3Ut9ofWf6MaPTPduJbQdbmAO27uP7/ACrnYyllYEEYIPqDBq0JVJBJWiOwrg049Nmu3Uc+kFFSXtsLY3DHMBdxzxgcd6W3axAPUAW6ckR3wak2NNIL7lT2bjJHMDJ459BPFeZ4nMpOuiOnHDTuViQX27fWACVIxnJP9/Spuk0u66PNXaknew6mA9uZJhRPafarmzotlvcOslhhY49Tt7TP4/WnNQLbdD2grHgF2hZBAyOBxk+9cv1uiRWle7MWdK7f5hUf7R/UU+lpQMAe+In6xXaNuWcfdQRXtQhFbo45NnBtj0FFKRSRVkiTEpaWKULWhQkURUrT6UscCp3/AKM8E7T2qU80IOmykcUpcIpooqXf0hUwaZ200ZqStCuDXIxcOD9K9b8bAVtNuYdV+1tBtq/dQAC3y8E7hkTXk96AIJ7V674pfDNpmts21r1rKQZkrAPosRU5yTa7izjt3Ra6q2/lt5YJfcsAHaYxOfSKa8NtFU2MylhJbbkCWmMicVI8Q0t1rRFlwr7h80bSMTPfjGPXvVL4VY2MfNvbmOAMsNwYyoYDIhR+JEYiuU6epdX7cow4lWH0kVVaTwJlA8rUiO0qCcg/aVv5dqutyujbWBwwMcjkZ9KgWNEhQKmqX/lsY8D0IpZGlZa8Ds6cB9dq2dpBiSBKbjgCXPzevYVKHxDaC7dJaABJAJWJI/ZXLcdzXdv4f0hffevNdYZ2q5VREj5UM9+7VJOusWj5dm0qTwQFWSSOwyx+tT/BtlWP06/c2wVtbDO6EG8xAIHURE8gip2n+GgFm9dwAAQsKv3se34V0L1x2K2w6vvSJCAbdyl8ls9O6pb+EXbx62VAJBB3OY6gMGBEZwYmlSt8WZZB1P6BZ27gjFjC4a5uJMCOR2qvPxTpEYqtt5XB2W0H8SPSrvXeEWLRS67dVvKSVVA3EwZzE96odN4PoHuM5UFmYs0XHiTnAVoAknFMtudhotdSw0/xEjXHt+RcU20DNuCAbW27SIbgyI+tceIeNaZGRL1ojzEVlJRGXaTGczzjirkJYZYcjabaofmgomVBj03H3zVT4ppNM2wtbQ+UCLZZ3XYOT2gjvk4o68qjLQ7p9DpbwIs3AjdwjRkSM237TPAFVviPh2t08my4cTIUdDe/Sx2nt3+6p3gPgNli163cYNBUqCr25kHcMbpxnPrUbxXwvUJee5bvmGG+0gZtklyxVlJjK9Mj8qK60YnueZNfe3cLoxV0ckMpyCCc/wDWKkP4g90Rc2k5bcBDMSZO6MHJ7CorKxcC4yhi5D5EoS0MTIiMnI9DT+gtzLEEYOIyOCfrWxmtSKSjsQriUy52xM/379qn32blLZI9WwPypgvdJBtqy9BYhuCgMEgrmCemZjniuiWZaXpJ6GuSdp9PKmXVJUDJJJJjKtBxzMDtU4+DoRuF0jvD9xmSMQQSZmfyIltEhldmCsCzSGBC4np3DKk4zXWh1HmPtQ7oBjcCIM8byIBAgx2EGvJlqbux7SJmgt7rx37htiVtJEbRCgw0r3MHEsAO9SddpA4DeS+22GZ2YqhOCY/2jb/fep1+lv27bu15oXbtRIGcclTldxme01XeH+I303WrikhW61eWAOSZknmZJGTPOaxQclqTRnUz/htwIxDAwfcyPTHFW2wHg0ymgAjP5VKVK93HFxVMk9xDbAUEj0/Nopbmn7ANPOASY9YqVctE2wfdP/MU5qNKXbERtGSrMJn9kjP9xU8mbRyxo49XQr0RQOomf2hBp+2imIn7xirJfC0FsMd4ck9jsjiROffmlTR5UKobPI7ZwYHAz3/oa4JeNk9ollhS5JXhdgJDFgRPAH0/+61jeL2dnlbVDRgnBn3mqLReCqi+ZqCyFWgbGw4ILAKPTIPBEYNUt6w73D5cASAOZjqknGMnmvPywlllbkzoi4pUkRvG7m+5uXEnOR29aj6HQtdcICAxIABnM8cCpt/RkW5JIJMEL1dQiQCDJkdjimrOmuBmVUdgI3bdw5gAFvr+M114ZzxxpMnkUZO2Vep0JLhVZXJnpUmcD3UV6hom/wAlCH3LeshoO2IgksAsFYz9I4EVhbC/44PlSBgBSsgwA3JzOcGBLYitAPG0Z7V3bncgl0uHbsCjpkweoEkA11RyPls5skLSSR6J4v4Wt6wyYDEnaST0nbExnseKqvE9EiKhIVlDnewCkQ7SBB5h9mfQH1qFf8ca4gFu1d2wRMEEzxMDFVesbVXrZs2ldfLR3h0Us0HcJYkfa9vSmU7dIHGt2XfgOq3I6KsKA8mQRgmduJLRkmYHvULUeFaCzcN19YyOFNtgXWBJBI2lYmRVFp/DdSgW4NSBv6zNvgON0MJEmGiD7egqD4n4i6h0N9zCkEeTAAIM/wD9Mc+/allJ8JJ/LoxSi3ybPwcaPcNpe4u0yzEqDknttUiZ59Ks9bq7aKFspbXf8pMAAepCAknnkisP4NrbToD17lgKTtiRJAgSfskzGIq101hbtwb/ACgAoyCzQrywlU2DJJ5P9KJKVPdL4FTdlit4oQ7aiIg9CKPbly0/hVi993RXt3uSWbc6ztAIB2j324Ap9PDA1rygwFsncIRREAcbp+ufU1iPiXSm1fRNOGdI3PJXKAsrGQsDKjgd/eudQlHzSk3+B3vtRa6jx5EfqJcAGAdgO6ZLZk00/wAZ42rb787x9Iwvt61VeLaMW7RvuqKsJsDF2Dlk3jaA4kGCO2QazVvx8hzv09h12iEVWBmF+2xIkZxHtU/pwnvu/llk6/Y9A0HxRYIK3umYyBv4g8EVG8U+K7KH/AuOfUkbV9IEAnH86xup8eW6nlW9DbQkwrj5oBnHSP7NWvhV3TvaRYi4A5clFZebm0BiM428+tDUYxqvyarcrstfDPiy210b7gWCGJJdpAyRETnH9iq7xbUXGvO+nvxLdIVrigqZh1BEes/d61D8fewrIiYcFI2rG/eiEyO4BJ9Zx61TXH2FlDMfTmf3fuNCemNRHUFJ2yQnht8yXRGPJIdQXJOSYOTySDH8qk6ZHQNvQjpJyMHABGR7n8KkJtGWiJM4HTgsDHYQJqUmpthLfX1kQ2YUSQOkj19faprLK7aKuMaoi67x4eXasMqBQJMKxPrnbBOCDg/hWLuaxuVG4gdON23Mynpknj1PrW08Utpgh4Yqs7AQvAEgEZ4k/Woz+HblQ73DGAzFhtAPyyvMSRkE4mQK6MLVUkSlG+WY22r3WZgjOZktEknnPvWi+GWc3/0RQEa422HHQrjJmB6LEkHt709oNOUZWuGA2xQAw+dmWWMcrAjNXPhrhLolydt0FR9kHcBuEiZ2zjGJq2RR9LE0NKxpfBbjXAjXBv3hcBwi7jtJ6Ug/cfX0qmfRObu03W6nCye5bjjOOc+tXOq17Nf1CO93d5lwhFcsAu5woCkRGF7Rg800mk3tYuCAN4BAncIcfXAAkycfjXPJxTSS/wBlMWNtNlW7pMAFieAompvhuk805hFgkMer1ABAIjIrSaX4bZjAtMznPWu0es78EnjuOajv4M6OzBG5bCnAjtPeDj6A+tdWXNNryuiMYxT3Kc6p0RrTW0aQIYSSMyIzg1GR3VusgYHoStae54TucMlklVbIG4lhzyMe1VnieiJLOXVct0gSwImMgAciO3IxXM1J+p2VjJLhEfTXt56QXJ7c8fRYmpa6l0cb12wcleeB2ODEg/XvTvhWlvoQyoWWJVgp9CwgsM84xzUq/wCC3rgdnLyZyEL8d5JkmO0iSKxYoVvyEpNsuLai9pwQ42ZJP2xAltwMiQJ4BmRI4rnT2NwRrlpEQDpHG4kErJJyekGTjGB2qr8Jc2bgBK4VFIdYcsXI6ZG4HvA496tvFLytb3rcRiGZUJJL7CftMffuDBmspRdGWT9VpEW272kUdDkEmGmIJAnsBnHftxUO5olDKVsqpuDEPbVTtCkfMp7OOKpX+JFF9x+kMVBZUCcRvMA4mMfwpux4slwIjXHd1naAzNsUkARjA2r6eldE3DTsqsnBO9ybf0h80q93bJUjaFcZGQSJxUPUaXTrbP8AiYV3C7EiJYNPbgOe/wBn8K7xnSvKNb8xSCN0q4mJMTIMHcRBBxUC26OfLFtiJODuMSZIEGYwv4VFRtWmUcvYvbWqcW9tvVMEdLhVdoG4qxUBgxb0OParPwoFEBB3OS24o0DYSBtPIDSGzHcYxWFTUuhx4ZPu1y4cARGZApU8Z1KIyrpAiszMdu/JY7jIB5mDM1SGKcZarJ5WpRaqjZJ4nZ/R7akMX8tcNcEAqg9EknjHvWO+IdcpYzbQlpk4wDGQe5Ef2JBi3711WPl6S3wrTsLdRRQZYx3kR9RTd/U6reSmnsL1GG8tNxAOCSw594rok1Lc5FglGVpm58Ds6dtObljUbbltNzhEQAOUIElgd8ywMdzFdeF6gG4EfMoQQhHXsXpGAQOy/Ssf4TrNSjf4luyykGZW3K4gRBEg95q4fxG8fkt2fTqCAcexNc0Y/Tk3dpnUoNrcnPbV7ZZkLN1FZDN8uO+Jkip2tZEvqwtb0FtAVYHaSxBIJUYABzj1xVG2r1LKCbltCBHQqsPUxKE81ymrbPm3Gf0y6RzxsK+vf0pXw03djrFvdlp4pa3rsuRs2ZKdSr0zKzBUqJH41l08Fcbkt3k2lA8tAMBgdh2znEzjj8bUX7TdMp69YLA88lmPrTGoNpWFy2LUgjCdAx/tK4PtRGoqkbKPcgW/C1Llbt3YvBbaYmfQj35OMTV54Re01q2EZVLr1GArM37THPAb2+amf/Wi0m5prDmOgnZKkdzMhhMGMcR3kc3/ABu623alpCPtK6nsBMbIWYmBialOLkqf6jR23Otf5Ltb2KehgSCw6UK7AViNonZj7hR4ppUZkdLYhVcXG35IYoVPXADTiZ4bvUW5cDHLKpP6r49YwRimdS0jbuUZ+ZWVSY9SgOPY0RhTW/8AzGb2Jou3Gcu2lRQ6leq+rAgARhBx7iefeuFJMI1tJVSFFslyu/8AWZvs9M/cfXNY6PILXOBAJuF+eYCcfhS29dcQnYMnkgATHcmJP300/wC1BFf1M1C6rTqotvZUsOWKXXJCgAK0GDPP31Bt6dEuIwN54eRIKrB+yVYEMMkZIwBUSz4o7Abr7Ie48otH3zTq3LZMvq3PB60Kj8IH4Glj9TY2o8jqiwyG2bTqcwzXMAAlhI2ycY5qR4hq7LIPJtlD3JZcksJgke3qKhi3pO+oSfUnP/lNO6m3p3UAahBtBIh0WT2EkyKtGVepiyX2RLutbbWOVRAQzBz3lm8wH6w30z7VP8auadFTyHcdiAUOGZZBAUegn2M5jFNb8MtOy3HvlCAAVS+rK0ACWMSTgd4xS6vwDTOJW47bsmbkqSMzAicitWWCVWRljm5WW6fFL7VR2wFAj1j3FXeg+LECwLbMP2mL/gTwK82snpH1/lWw06AKIAHT2x3rZeXgfSmWuu+KUZehdh9QSpP4Vktf4lvB6mbPAcz/ABpdXhnAwPTt+FZvVfMayC3s3hUbj4Z8dW2YYMuI+Yj8SavrvxQqmdzR/u/oa8x0Wec/XNP6nHGPpisneo1VXBrdV8SgsxkQSZwGP51Af4ix03GHsBt/gKy803cuGeT+NbGKsx8Go8P8RV7g8y7cCk5IGR+dX/kaP7OsuL9x/gDXndP2zV5bIQ2t3wyw/HiP7yOPzmox+HAf8vxDT/e5X+INZpuKZFw+p/GsTobT7mnb4Svt8uosv/tuD+a1mNfp2t3GtuwLKYMMGE+xHNPbzt5NVt3mmixGmLj+xSQp5AP/ABFN11a5pnFUYSLW0fZA/wCIqXbvx2I+igj+IqE/IocYFRlHc2y2/SIEdXr8g/8AlTD3177fvU/0qD/1QwqekZMf/Sh+qv7oroatf1V/dFQW5/v0rjvSuCHTLQatf1U/dH9K6GqU8qn7oqsahaVxQ6LXzFP2U/dFBdP1F/AVXCilo0sA6f6a/hXQdP1F/Cq4V2tDAn70/UH50o2HhJ+kmoApaUYnbB/p/kaNg/0//bUGlFFATvLXvZH7n/VcXLCf6I/cqMKTzG/WP4mtFP/Z" class="card-img-top" alt="...">
                    </div>
                    <div class="card-body">
                     <?php echo '<h5 class="card-title">'.$row['Hotel_name'].'</h5>'; ?>
                            <p class="card-text">
                <?php
                      echo "<b>Hotel Address: </b>".$row['Hotel_addr']."<br>";
                      echo "<b>Hotel Price: </b>".$row['Price']."<br>";
                      if (array_key_exists('Features', $row)) {
                        echo "<b>Hotel Features: </b>".$row['Features']."<br>";
                      } else {
                        echo "<b>Hotel Features: </b> Normal <br/>";
                      }
                      echo '<a href="#" class="btn btn-primary" onClick = "Redirect('.$row['ServiceID'].', '.$row['Price'].')">Book</a></div></div>';
                ?>
            </div>
        <?php
                }
            }
        }
        ?>
    </div> <!-- div.row -->
    </div> <!-- div.container -->
<script type="text/javascript">
         // <!--
            function Redirect(serviceID, price) {
               cookieHotel(serviceID, price);
               window.location.href="./bookHotel.php";
            }
         //-->
</script>
</html>
