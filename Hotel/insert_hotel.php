<?php
    include_once 'connectDB.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Hotel Inserting Result | AirMi</title>
</head>
<body>
<?php
        // check the cookie for stakeholder type and name
        $cookie_name = 'stakeholder';
        $partner = 'partner';
        if(!isset($_COOKIE[$cookie_name])) {
            echo "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            $stakeholder = $_COOKIE[$cookie_name];
        }
        if ($stakeholder == 'partner') {
            if(!isset($_COOKIE[$partner])) {
                echo "Cookie named '" . $partner . "' is not set!";
            } else {
                $partner_name = $_COOKIE[$partner];
            }
        }
         
        // since ID is NOT AUTO INCREMENT by default, we work around using this
        $sqlMaxId = "SELECT MAX(ServiceID) AS MaxID FROM `HOTEL_SERVICE`";
        $result = mysqli_query($conn, $sqlMaxId);
        $resultCheck = mysqli_num_rows($result);

        $maxID = 0;

        if ($resultCheck > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $maxID = $row['MaxID'] + 1;
            }
        }

        // get the value from the POST value
        $rank = intval($_POST['rank']);
        $no_rooms = intval($_POST['no_rooms']);
        $no_avai_rooms = $no_rooms;
        $hotel_addr = $_POST['hotel_addr'];
        $hotel_name = $_POST['hotel_name'];
        $price = floatval($_POST['price']);
        $city = $_POST['city'];
        $country = $_POST['country'];

        $insertCode = "INSERT INTO `HOTEL_SERVICE` VALUES ($maxID, $rank, '$hotel_addr', '$hotel_name', $price, $no_rooms, $no_avai_rooms,
        '$partner_name', '$city', '$country');";
        $result = mysqli_query($conn, $insertCode);
        if ($result === TRUE) {
            echo "<div class='alert alert-success'>Your hotel code {$maxID} is successfully added.</div>";
        } else {
            echo "<div class='alert alert-danger'>Your hotel code {$maxID} receives an error. Please kindly wait we are fixing the errors</div>";
        }
    ?>
    <div>
        <a class='btn btn-primary px-4' href = "add_hotel_service.php">Add another</a>
        <a class='btn btn-secondary px-4' href='../Account/login_processing.php'>Back to homepage</a>
    </div>
</body>
</html>