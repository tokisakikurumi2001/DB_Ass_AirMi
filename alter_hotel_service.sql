CREATE TABLE `HOTEL_SERVICE` (
  `ServiceID` int(11) NOT NULL,
  `Rank` int(11) NOT NULL,
  `Hotel_addr` varchar(100) NOT NULL,
  `Hotel_name` varchar(50) NOT NULL,
  `Price` decimal(8,2) NOT NULL,
  `No_of_rooms` int(11) NOT NULL,
  `No_available_rooms` int(11) NOT NULL,
  `Provided_by` varchar(30) NOT NULL,
  `City` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `HOTEL_SERVICE`
--

INSERT INTO `HOTEL_SERVICE` (`ServiceID`, `Rank`, `Hotel_addr`, `Hotel_name`, `Price`, `No_of_rooms`, `No_available_rooms`, `Provided_by`, `City`, `Country`) VALUES
(1, 5, '345 This street, Known city, Good country', 'The east gem', '100.00', 400, 100, 'Khoi2001', 'Ha Noi', 'Vietnam'),
(2, 4, '678 Google, Mountview city, The country', 'The engine', '200.00', 400, 100, 'Khoi2001', 'Da Nang', 'Vietnam'),
(3, 5, '678 Apple, Bottom city, America country', 'The elegant hotel', '300.00', 400, 50, 'PNHuy2001', 'Da Nang', 'Vietnam'),
(4, 3, '678 Netflix, Lakeview city, America country', 'The amazing experience', '400.00', 400, 100, 'PNHuy2001', 'Long An', 'Vietnam'),
(5, 9, '540 CMT8 Street, Ward 10, Ho Chi Minh City', 'Meta', '9.00', 25, 14, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(6, 2, '98 Pham Hung Street, Ward 8, Ho Chi Minh City', 'Alphabet', '8.00', 20, 12, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(7, 4, '78 Ly Thai To Street, Ward 3, Ho Chi Minh City', 'Amazon', '13.00', 20, 19, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(8, 6, '1 Le Loi Street, Ward 9, Ho Chi Minh City', 'Alibaba', '11.00', 30, 20, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(9, 8, '5 Quang Trung Street, Ward Go Vap, Ho Chi Minh City', 'Tiki', '12.00', 40, 10, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(10, 10, '100 Nguyen Hue Street, Ward 1, Ho Chi Minh City', 'Shoppee', '16.00', 50, 0, 'Khoi2001', 'Ho Chi Minh', 'Vietnam'),
(11, 9, '123, Dau Con Rong', 'Ho Con Rua', '400.25', 200, 200, 'Khoi123', 'Ho Chi Minh City', 'Vietnam');
