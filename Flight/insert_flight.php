<?php
    include_once 'helper/dbconnect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Flight Inserting Result | AirMi</title>
</head>
<body>
<?php
        // check the cookie for stakeholder type and name
        $cookie_name = 'stakeholder';
        $partner = 'partner';
        if(!isset($_COOKIE[$cookie_name])) {
            echo "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            $stakeholder = $_COOKIE[$cookie_name];
        }
        if ($stakeholder == 'partner') {
            if(!isset($_COOKIE[$partner])) {
                echo "Cookie named '" . $partner . "' is not set!";
            } else {
                $partner_name = $_COOKIE[$partner];
            }
        }
         
        // since ID is NOT AUTO INCREMENT by default, we work around using this
        $sqlMaxId = "SELECT MAX(ServiceID) AS MaxID FROM `FLIGHT_SERVICE`";
        $result = mysqli_query($conn, $sqlMaxId);
        $resultCheck = mysqli_num_rows($result);

        $maxID = 0;

        if ($resultCheck > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $maxID = $row['MaxID'] + 1;
            }
        }

        // get the value from the POST value
        $rank = intval($_POST['rank']);
        $no_seats = intval($_POST['no_seats']);
        $no_avai_seats = $no_seats;
        $brand = $_POST['brand'];
        $aircraft_type = $_POST['aircraft'];
        $price = floatval($_POST['price']);
        $date_depart = $_POST['date_depart'];
        $date_return = $_POST['date_return'];
        $place_depart = $_POST['place_depart'];
        $place_arrive = $_POST['place_arrive'];

        $insertCode = "INSERT INTO `FLIGHT_SERVICE` VALUES ($maxID, $rank, $no_seats, $no_avai_seats, '$brand', '$aircraft_type', $price, '$date_depart',
        '$date_return', '$place_depart', '$place_arrive', '$partner_name');";
        $result = mysqli_query($conn, $insertCode);
        if ($result === TRUE) {
            echo "<div class='alert alert-success'>Your flight code {$maxID} is successfully added.</div>";
        } else {
            echo "<div class='alert alert-danger'>Your flight code {$maxID} receives an error. Please kindly wait we are fixing the errors</div>";
        }
    ?>
    <div>
        <a class='btn btn-primary px-4' href = "add_flight_service.php">Add another</a>
        <a class='btn btn-secondary px-4' href='../Account/login_processing.php'>Back to homepage</a>
    </div>
</body>
</html>