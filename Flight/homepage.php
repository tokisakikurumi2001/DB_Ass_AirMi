<?php
    include_once 'helper/dbconnect.php';
?>

<!-- Search for the flight-->

<!-- form stuff -->
<!-- action send to search_flight.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Flight Search | AirMi</title>
</head>
<body>
    <?php
        $sql = "SELECT DISTINCT(`Depature_place`) FROM FLIGHT_SERVICE;";
        $result_departure = mysqli_query($conn, $sql);
        $resultCheck_departure = mysqli_num_rows($result_departure);

        $sql = "SELECT DISTINCT(`Arrival_place`) FROM FLIGHT_SERVICE;";
        $result_arrival = mysqli_query($conn, $sql);
        $resultCheck_arrival = mysqli_num_rows($result_arrival);
    ?>
    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h1 style="text-align: center;">AirMi Flight Booking Service</h1>
            <form action="./search_flight.php" method="POST">
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="no_passengers">Number of passenger</label>
                        <input type="number" class="form-control" id="no_passengers" placeholder="Number of passenger" name="no_passengers" required>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="date_depart">Date depart</label>
                        <input type="date" class="form-control" id="date_depart" name="date_depart" required>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="date_return">Date return</label>
                        <input type="date" class="form-control" id="date_return" name="date_return">
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="place_depart">Place depart</label>
                        <select class='form-control' id="place_depart" placeholder="Place depart" name="place_depart" required>
                            <?php
                                if ($resultCheck_departure > 0) {
                                    while ($row = mysqli_fetch_assoc($result_departure)) {
                                        echo "<option value='{$row['Depature_place']}'>{$row['Depature_place']}</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="place_arrive">Place arrive</label>
                        <select class='form-control' id="place_arrive" placeholder="Place arrive" name="place_arrive" required>
                            <?php
                                if ($resultCheck_arrival > 0) {
                                    while ($row = mysqli_fetch_assoc($result_arrival)) {
                                        echo "<option value='{$row['Arrival_place']}'>{$row['Arrival_place']}</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="d-grid gap-2 col-2 mx-auto my-1">
                    <button type="submit" class="btn btn-primary">Find</button>
                    <a class='btn btn-secondary py-2' href='../Account/login_processing.php'>Back to homepage</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>