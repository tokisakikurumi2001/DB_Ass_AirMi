<?php
    include_once 'helper/dbconnect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Flight Booking | AirMi</title>
</head>
<body>
    <?php
        $cookie_name = 'booking_flight_service';
        if(!isset($_COOKIE[$cookie_name])) {
            echo "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            $serviceID = $_COOKIE[$cookie_name];
        }
         
        $sqlMaxId = "SELECT MAX(BookingID) AS MaxID FROM `FLIGHT_BOOKING`";
        $result = mysqli_query($conn, $sqlMaxId);
        $resultCheck = mysqli_num_rows($result);

        $maxID = 0;

        if ($resultCheck > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $maxID = $row['MaxID'] + 1;
            }
        }

        // POST data
        $luggage_weight = $_POST['luggage_weight'];
        $payment = $_POST['payment'];

        // cookie data
        $discount = 0.05;
        $price = intval($_COOKIE['unit_price']);
        $no_passenger = intval($_COOKIE['no_passengers']);
        $total_price = $price * $no_passenger * (1 - $discount);
        $seatClass = $_COOKIE['seat_class'];
        $insurance = $_COOKIE['insurance'];

        $stakeholder = $_COOKIE['stakeholder'];
        if ($stakeholder == 'customer') {
            $user = $_COOKIE['customer'];
        } else {
            die("Error occured. This stakeholder cannot book any service. Only user can book service\n");
        }
        
        $insertCode = "INSERT INTO `FLIGHT_BOOKING` VALUES ($maxID, $discount, $total_price, '$payment', '$insurance', '$seatClass', $no_passenger,
        '$user', $serviceID, NOW(), 'CHECKING')";
        $result = mysqli_query($conn, $insertCode);
        if ($result === TRUE) {
            if ($luggage_weight == NULL) {
                echo "<div class='alert alert-success'>Your booking code {$maxID} is checking. Please kindly wait</div>";
            } else {
                $sqlMaxId = "SELECT MAX(`LuggageID`) AS MaxID FROM `FLIGHT_LUGGAGE` WHERE FlightID = $maxID;";
                $result = mysqli_query($conn, $sqlMaxId);
                $resultCheck = mysqli_num_rows($result);

                $maxLuggageID = 0;

                if ($resultCheck > 0) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $maxLuggageID = $row['MaxID'] + 1;
                    }
                }
                $insertLuggage = "INSERT INTO `FLIGHT_LUGGAGE` VALUES ($maxID, $maxLuggageID, $luggage_weight);";
                $result = mysqli_query($conn, $insertLuggage);
                if ($result === TRUE) {
                    echo "<div class='alert alert-success'>Your booking code {$maxID} is checking. Please kindly wait</div>";
                    echo "<div><a class = 'btn btn-primary' href = '../Account/login_processing.php'>Back to home page</a></div>";
                } else {
                    echo "<div class='alert alert-danger'>Your booking code {$maxID} receives an error. Please kindly wait we are fixing the errors</div>";    
                }
            }
        } else {
            echo "<div class='alert alert-danger'>Your booking code {$maxID} receives an error. Please kindly wait we are fixing the errors</div>";
        }
    ?>
</body>
</html>