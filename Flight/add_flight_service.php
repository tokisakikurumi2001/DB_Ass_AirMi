<!-- Add the flight service -->

<!-- form stuff -->
<!-- action send to insert_flight.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Flight Add | AirMi</title>
</head>
<body>
    <div class="container">
        <div class="shadow p-3 bg-body rounded">
            <h1 style="text-align: center;">New AirMi Flight Service</h1>
            <form action="./insert_flight.php" method="POST">
                <!-- RANK -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="rank">Rank</label>
                        <input type="number" class="form-control" id="rank" placeholder="Rank of services" name="rank" value = "9" required>
                    </div>
                </div>
                <!-- NUMBER OF SEATS -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="no_seats">Number of seats</label>
                        <input type="number" class="form-control" id="no_seats" placeholder="Number of seats" name="no_seats" value = "40" required>
                    </div>
                </div>
                <!-- BRAND -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="brand">Brand</label>
                        <input type="text" class="form-control" id="brand" placeholder="Brand" name="brand" required>
                    </div>
                </div>
                <!-- AIRCRAFT TYPE -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="aircraft">Aircraft type</label>
                        <input type="text" class="form-control" id="aircraft" placeholder="Aircraft type" value = "Boeing737" name="aircraft" required>
                    </div>
                </div>
                <!-- PRICE -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="price">Price of 1 seat</label>
                        <input type="number" step=0.01 class="form-control" id="price" placeholder="Price of 1 seat" name="price" required>
                    </div>
                </div>
                <!-- DATE DEPART -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="date_depart">Date depart</label>
                        <input type="date" class="form-control" id="date_depart" name="date_depart" value = "2021-12-09" required>
                    </div>
                </div>
                <!-- DATE RETURN -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="date_return">Date return</label>
                        <input type="date" class="form-control" id="date_return" name="date_return" value = "2021-12-12" required>
                    </div>
                </div>
                <!-- PLACE DEPART -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="place_depart">Place depart</label>
                        <input type="text" class="form-control" id="place_depart" placeholder="Place depart" name="place_depart" value = "Ho Chi Minh City" required>
                    </div>
                </div>
                <!-- PLACE ARRIVE -->
                <div class="row justify-content-md-center">
                    <div class="col-md-3">
                        <label class="form-label" for="place_arrive">Place arrive</label>
                        <input type="text" class="form-control" id="place_arrive" placeholder="Place arrive" name="place_arrive" value = "Ha Noi" required>
                    </div>
                </div>
                <div class="d-grid gap-2 col-2 mx-auto my-1">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <a class='btn btn-secondary py-2' href='../Account/login_processing.php'>Back to homepage</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>