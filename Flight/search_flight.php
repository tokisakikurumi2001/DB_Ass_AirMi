<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    
    include_once 'helper/dbconnect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Flight Searching Result | AirMi</title>
    <script>
        // LINK: https://www.quirksmode.org/js/cookies.html
        function createCookie(name,value,days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
            }
            else var expires = "";
            document.cookie = name+"="+value+expires+"; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            createCookie(name,"",-1);
        }

        function whichSeatClass(price) {
            price = parseInt(price);
            if (price >= 400) {
                return '1stClass';
            } else if (price >= 300) {
                return '2ndClass';
            } else if (price >= 200) {
                return '3rdClass';
            } else {
                return 'Normal';
            }
        }

        function whichInsuranceType(price) {
            price = parseInt(price);
            if (price >= 400) {
                return 'Type A';
            } else if (price >= 300) {
                return 'Type B';
            } else if (price >= 200) {
                return 'Type C';
            } else {
                return 'No plan';
            }
        }

        function book(serviceID, no_passenger, price) {
            createCookie('booking_flight_service', serviceID, 1);
            createCookie('no_passengers', no_passenger, 1);
            seatType = whichSeatClass(price);
            createCookie('seat_class', seatType, 1);
            insuranceType = whichInsuranceType(price);
            createCookie('insurance', insuranceType, 1);
            createCookie('unit_price', price, 1);
        }
    </script>
</head>
<body>
    <div class="container">
    <div class="row row-cols-1 row-cols-md-3 g-4">
        <?php
            $no_passenger = intval($_POST['no_passengers']);
            $date_depart = $_POST['date_depart'];
            $date_return = $_POST['date_return'];
            $place_depart = $_POST['place_depart'];
            $place_arrive = $_POST['place_arrive'];
            if ($date_return == NULL) {
                $sql = "SELECT * FROM `FLIGHT_SERVICE` WHERE\n"
                . "`No_available_seats` > $no_passenger\n"
                ."AND `Date_depart` = '$date_depart'\n"
                ."AND `Depature_place` = '$place_depart'\n"
                ."AND `Arrival_place` = '$place_arrive'\n"
                .";";
            } else {
                $sql = "SELECT * FROM `FLIGHT_SERVICE` WHERE\n"
                . "`No_available_seats` > $no_passenger\n"
                ."AND `Date_depart` = '$date_depart'\n"
                ."AND `Date_return` = '$date_return'\n"
                ."AND `Depature_place` = '$place_depart'\n"
                ."AND `Arrival_place` = '$place_arrive'\n"
                .";";
            }
            $result = mysqli_query($conn, $sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck > 0) {
                $i = 1;
                while ($row = mysqli_fetch_assoc($result)) {
                    # NOTE: row is now an associate array, and every value is of string type
                    echo "<div class=\"col\">\n<div class=\"card shadow\">\n";
                    echo "<img src='assets/AirMi.png' class=\"card-img-top\"/>\n";
                    echo "<div class=\"card-body\">\n";
                    echo "<h5 class=\"card-title\">Flight number $i</h5>\n";
                    echo "<p class=\"card-text\">Brand: {$row['Brand']}</p>\n";
                    echo "<p class=\"card-text\">Rank: {$row['Rank']}</p>\n";
                    echo "<p class=\"card-text\">Aircraft type: {$row['Aircraft_type']}</p>\n";
                    echo "<p class\"card-text\">Price per seats: {$row['Price']}</p>\n";
                    echo "<p class=\"card-text\">Date depart: {$row['Date_depart']}</p>\n";
                    echo "<p class=\"card-text\">Date return: {$row['Date_return']}</p>\n";
                    echo "<p class=\"card-text\">Departure place: {$row['Depature_place']}</p>\n";
                    echo "<p class=\"card-text\">Arrival place: {$row['Arrival_place']}</p>\n";
                    echo "<p class=\"card-text\">Partner: {$row['Provided_by']}</p>\n";
                    echo "<button type=\"button\" class=\"btn btn-primary\" data-bs-toggle=\"modal\" data-bs-target=\"#staticBackdrop{$i}\"
                    onclick='book({$row['ServiceID']}, {$no_passenger}, {$row['Price']})'>BOOK</button>";
                    echo "</div></div></div>";
                    // could open the boostrap modal -> make a form in modal -> make cookie for serviceID
                    // send form to final state.
                    echo "<div class=\"modal fade\" id=\"staticBackdrop{$i}\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel{$i}\" aria-hidden=\"true\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                            <h5 class=\"modal-title\" id=\"exampleModalLabel{$i}\">Flight service {$i}</h5>
                            <button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
                            </div>
                            <div class=\"modal-body\">
                                <form action='flight_book_result.php' method='POST'>\n
                                <div>
                                    <label>Luggage weight: </label>
                                    <input type='text' name='luggage_weight'/>
                                </div>
                                <div>
                                    <label for = 'payment'>Payment method</label>
                                    <select id = 'payment' name = 'payment'>
                                        <option value = 'CreditCard'>CreditCard</option>
                                        <option value = 'Cash'>Cash</option>
                                        <option value = 'Momo'>Momo</option>
                                        <option value = 'ZaloPay'>ZaloPay</option>
                                    </select>
                                </div>
                                <div>
                                    <input type='submit' class = 'btn btn-primary' value='book'/>
                                </div>
                                </form>
                            </div>
                            <div class=\"modal-footer\">
                            <button type=\"button\" class=\"btn btn-secondary\" data-bs-dismiss=\"modal\">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>";
                    $i += 1;
                }
            }
        ?>
    </div>
    </div>
</body>
</html>